import React from "react";
import { Switch, Route } from "react-router";

export default (
    <Switch>
        <Route exact path="/" />
          <Route exact path="/userprofile"  />
          <Route exact path="/user/:username"  />
          <Route exact path="/post/:title" />
          <Route exact path="/edit-profile"  />
          <Route exact path="/tag/:query"  />
          <Route exact path="/category/:query"  />
          <Route exact path="/topic/:query"  />
    </Switch>
);