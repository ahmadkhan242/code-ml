import React from "react";
// import "./App.css";
import {
  BrowserRouter as Router,
  Route,
  Switch,

} from "react-router-dom";

import Dashboard from "./components/dashboard/dashboard";
import TopicsPage from "./components/topics/main";
import POSTMAIN from "./components/post/show_post";
import EDITPROFILE from "./components/auth/edit_profile";
import PROFILE from "./components/profile/profile";
import USERPROFILE from "./components/profile/user_profile"
import ReactGA from 'react-ga';
import { createBrowserHistory } from 'history';

function App() {
  ReactGA.initialize('UA-162024881-1');
  const history = createBrowserHistory();
  history.listen(location => {
    ReactGA.set({ page: location.pathname }); 
    ReactGA.pageview(location.pathname); 
  });
  return (
    <div className="App">
      <Router history={history}>
        <Switch>
          <Route exact path="/" component={Dashboard} />
          <Route exact path="/userprofile" component={USERPROFILE} />
          <Route exact path="/user/:username" component={PROFILE} />
          <Route exact path="/post/:title" component={POSTMAIN} />
          <Route exact path="/edit-profile" component={EDITPROFILE} />
          <Route exact path="/tag/:query" component={TopicsPage} />
          <Route exact path="/category/:query" component={TopicsPage} />
          <Route exact path="/topic/:query" component={TopicsPage} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
