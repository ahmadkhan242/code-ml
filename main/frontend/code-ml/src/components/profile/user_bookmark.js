import React, { Component } from "react";
import {
    Row,
    Col,
    Image,
    Badge,
    Card,
  } from "react-bootstrap";

  
import Moment from 'react-moment';
import { get_user_profile } from '../funtion/user_funtion'
import {  MdInsertComment} from 'react-icons/md';
import { IconContext } from "react-icons";
import {  GiBookmark} from 'react-icons/gi';
import {  TiLightbulb} from 'react-icons/ti';

class USERBOOKMARK extends Component {

    constructor(props) {
        super(props);
        this.state = {
            first_name: '',
            second_name: '',
            email_id: '',
            country: '',
            username: '',
            thumbnail: '',
            bio: '',
            github: '',
            linkedin: '',
            personal_link: '',
            twitter: '',
            work: '',
            verified: '',
            bookmarks:[],
            post_star:[],
            comment_star:[],
            post_visit:[],
            createdAt:"",
            show:false,
            userId: ""
        };
        //this.post_bookmarks = this.post_bookmarks.bind(this);
      }
    //   fetch_bookmarks(){
    //   get_user_profile({userId:this.props.p})
    //   .then(res => {
    //     this.setState({
    //       bookmarks: res.bookmark
    //     })
    //   })
    //   .catch((e)=>{
    //       console.log(e);
    //     })
    // }


    //   post_bookmarks(e){
    //     const newUser = {
    //       userId: this.props.p,
    //       postId: e.id
    //     }        
    //     post_bookmark(newUser)
    //       .then((res) => {
    //        this.fetch_bookmarks()
    //       }
    //       )
    //       .catch((e)=>{
    //         console.log(e);
    //       })
    //   }
      
    componentDidMount() {
         this.setState({bookmarks: this.props.bookmark})
    }
  render() {
    let post_bookmark = this.props.post_bookmark;
    return (
      <div className="user_bookmark">
        <div style={{ margin:"auto"}}>
          <h3 className="bookmark__title"><span>Bookmarks</span></h3>
          <hr className="bookmark__hr"/>
        {this.state.bookmarks.map(function(object, i){
            return <div key={i}>
            <Card className="bookmark__card">
                      <Card.Body className="bookmark__cardBody">
                        <Row className="bookmark__cardRow">
                          <Col  xs={12} md={4}  className="bookmark__colImg">
                          <Image
                            style={{marginLeft:"0"}}
                            src={object.post.cover_img == null ?" " :object.post.cover_img}
                            width="100%"
                            height="100%"
                            className="logoone"
                            rounded = "true"
                          />
                          </Col>
                          <Col  xs={12} md={8} style={{ padding:"0"}} className="bookmark__colBd">
                            <div style={{display:"flex", justifyContent:"start", marginTop:"10px"}}>
                            <Image
                            style={{marginLeft:"0"}}
                              alt=""
                              src="https://www.w3schools.com/howto/img_avatar.png"
                              width="45px"
                              height="45px"
                              className="logoone"
                              roundedCircle = "true"
                            />  
                            <Card.Title className="bookmark__cardTitle">
                              <a style={{ textDecoration:"none"}} href={"/post/"+ object.post.title}><p style={{color:"#F2AA4CFF",fontFamily:"charter", fontSize:"23px", fontWeight:"bold"}}>{object.post.title==null?`No post Found`:object.post.title}</p></a>
                            </Card.Title>
                            </div>
                            <Card.Text className="bookmark__text">
                                <p style={{color:"#f2f2f2",fontFamily:"normal"}}><Moment format="D MMM YYYY" withTitle date={object.post.createdAt} /></p>
                                
                                  tags - {object.post.tags.map(function(o, i){
                                    return <span key={i}><a  href={`/tag?search=${o}`} style={{color:"#F2AA4CFF", margin:"5px"}}><Badge pill style={{backgroundColor:"black"}}>#{o}</Badge></a></span>;
                                  })}
                            </Card.Text>
                            <footer className="bookmark__cardFooter">
                              <div>
                                <IconContext.Provider value={{ color: "#f2f2f2",size:"20px", className: "global-class-name" }}>
                                <TiLightbulb/>
                                </IconContext.Provider>
                                <span style={{color: "#f2f2f2",paddingRight:"10px", paddingLeft:"5px"}}>{object.post.stars}</span>
                                <IconContext.Provider value={{ color: "#f2f2f2",size:"20px", className: "global-class-name" }}>
                                  <MdInsertComment/>
                                </IconContext.Provider>
                                <span style={{color: "#f2f2f2",paddingRight:"10px", paddingLeft:"5px"}}>{object.post.comment_count}</span>   
                                <span style={{color: "#f2f2f2", paddingLeft:"5px"}}>{object.post.read_time}</span>   
                              </div>
                              <div>
                                <IconContext.Provider value={{ color: "#f2f2f2",size:"20px", className: "global-class-name" }}>
                                  <GiBookmark style={{cursor:"pointer"}} onClick={()=>(post_bookmark({id : object.post.id}))}/>
                                </IconContext.Provider>
                                <span style={{color: "#f2f2f2",paddingLeft:"5px"}}>{object.post.bookmark_count}</span>
                              </div>
                            </footer>
                          </Col>
                        </Row>
                      </Card.Body>
                  </Card>
                  <hr className="bookmark__hr"/>
                  </div>
          })}
        </div>
      </div>
      
    );
  }
}

export default USERBOOKMARK;
