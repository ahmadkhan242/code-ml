import React, { Component } from "react";
import Moment from 'react-moment';

import {
    Image,
    Badge,
    Jumbotron
  } from "react-bootstrap";
import Navbar from "../layout/nav_bar";
import FOOTER from "../layout/footer";
import {  GoMarkGithub} from 'react-icons/go';
import {  FaLinkedin,FaTwitter,FaLink } from 'react-icons/fa';
import { IconContext } from "react-icons";
import {url} from "../../utils/config"

class PROFILE extends Component {

    constructor() {
        super();
        this.state = {
          user: []
        };
      }



      componentDidMount() {
      fetch(url+"/user/" + this.props.match.params.username)
          .then(res => res.json())
          .then((user) =>
          this.setState( {user} , () =>
              console.log("user fetched...")
          )
          ).catch((er) =>{
            console.log("Erorr", er);
          })
      }
  render() {
    var user_data = this.state.user;
      if(this.state.user.data){
        user_data = this.state.user.data;
      }
    return (
      <div style={{width:"100%",display:"flex",flexDirection:"column",justifyContent:"center", height:"100%"}}>
      <Navbar p={this.props}/>
      <div className="user">
      <Jumbotron className="profile__jumbo">
       
      <div className="user__main">
            <div className="user__user">
                <a href="/">
                    <Image
                    style={{margin:"auto"}}
                    src={user_data.thumbnail == null?"https://www.w3schools.com/howto/img_avatar.png": user_data.thumbnail }
                    width="234px"
                    height="234px"
                    alt={user_data.name}
                    className="logoone"
                    rounded = "true"
                    />
                </a>
                <div>
                    <p style={{marginTop:"60px",marginLeft:"15px"}}> <span style={{color:"#f2f2f2",fontFamily:"charter"}}>Joined : <Moment format="D MMM YYYY" withTitle date={user_data.createdAt} /></span></p>
                    {user_data.country == null? <p></p>:<p style={{marginTop:"10px",marginLeft:"15px",fontFamily:"charter", color:"#f2f2f2"}}>Location : {user_data.country}</p>}
                </div>
            </div>
            <div>
                <p style={{marginTop:"20px",marginLeft:"30px", fontSize:"30px",color:"#f2f2f2", letterSpacing:"0.10em",fontFamily:"charter"}}>{user_data.name}</p>
                <p style={{marginTop:"10px",marginLeft:"30px", fontSize:"20px",color:"#f2f2f2", letterSpacing:"0.05em",fontFamily:"charter"}}>@{user_data.username}</p>
                <p style={{marginTop:"10px",marginLeft:"30px", fontSize:"20px",color:"#f2f2f2", letterSpacing:"0.05em",fontFamily:"charter"}}>Email : {user_data.email_id}</p>
                <p style={{marginTop:"10px",marginLeft:"30px", fontSize:"20px",color:"#f2f2f2", letterSpacing:"0.05em",fontFamily:"charter"}}>About : {user_data.bio == null? "____":user_data.bio}</p>
                <p style={{marginTop:"10px",marginLeft:"30px", fontSize:"20px",color:"#f2f2f2", letterSpacing:"0.05em",fontFamily:"charter"}}>Work : {user_data.work == null? "____":user_data.work}</p>
                <div style={{marginTop:"20px",marginLeft:"30px"}}>
                  <IconContext.Provider value={{ color: "#f2f2f2" ,size:"20px", className: "global-class-name" }}>
                    <a href={user_data.github == null? '':'https://github.com/'+ user_data.github} rel="noopener noreferrer" target="_blank" style={{marginRight:"10px"}}><GoMarkGithub/></a>
                  </IconContext.Provider>
                  <IconContext.Provider value={{ color: "#f2f2f2" ,size:"20px", className: "global-class-name" }}>
                    <a href={user_data.linkedin == null? '':user_data.linkedin} style={{marginRight:"10px"}} rel="noopener noreferrer" target="_blank"><FaLinkedin/></a>
                  </IconContext.Provider>
                  <IconContext.Provider value={{ color: "#f2f2f2" ,size:"20px", className: "global-class-name" }}>
                    <a href={user_data.twitter == null? '':'https://twitter.com/'+ user_data.twitter} style={{marginRight:"10px"}} target="_blank"><FaTwitter/></a>
                  </IconContext.Provider>
                  <IconContext.Provider value={{ color: "#f2f2f2" ,size:"20px", className: "global-class-name" }}>
                    <a href={user_data.personal_link == null? '':user_data.personal_link} style={{marginRight:"10px"}} rel="noopener noreferrer" target="_blank"><FaLink/></a>
                  </IconContext.Provider>
                </div>
            </div>
          </div>
      </Jumbotron >
      </div>
      <FOOTER/>
    </div>
    );
  }
}

export default PROFILE;
