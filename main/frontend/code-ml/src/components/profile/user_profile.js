import React, { Component } from "react";
import USERBOOKMARK from "./user_bookmark"
import FOUR0FOUR from "../layout/404"

import {
    Image,
    Badge,
    Jumbotron
  } from "react-bootstrap";
  import jwt_decode from 'jwt-decode'
  import Moment from 'react-moment';
import Navbar from "../layout/nav_bar";
import FOOTER from "../layout/footer";
import {  GoMarkGithub} from 'react-icons/go';
import {  FaLinkedin,FaTwitter,FaLink } from 'react-icons/fa';
import { IconContext } from "react-icons";
import { post_bookmark } from '../funtion/bookmark_funtion'
import { get_user_profile } from '../funtion/user_funtion'

class USERPROFILE extends Component {

    constructor() {
        super();
        this.state = {
            name: '',
            email_id: '',
            country: '',
            username: '',
            thumbnail: '',
            bio: '',
            github: '',
            linkedin: '',
            personal_link: '',
            twitter: '',
            work: '',
            verified: '',
            bookmarks:[],
            post_star:[],
            comment_star:[],
            post_visit:[],
            createdAt:"",
            show:false,
            userId: ""
        }
      }
      log_out(e) {
        e.preventDefault()
        localStorage.removeItem('user_token')
        this.props.history.push(`/`)
      }

      fetch_bookmarks(e){
        get_user_profile({userId : e})
        .then(res => {
          this.setState({
            bookmarks: res.bookmark
          })
        })
        .catch((e)=>{
            console.log(e);
          })
      }
    componentDidMount() {
        const token = localStorage.user_token
        
        if(token !== undefined){
          const decoded = jwt_decode(token)
        this.setState({
          userId: decoded.id,
          name: decoded.name,
          email_id: decoded.email_id,
          country: decoded.country,
          username: decoded.username,
          thumbnail: decoded.thumbnail,
          bio: decoded.bio,
          github: decoded.github,
          linkedin: decoded.linkedin,
          personal_link: decoded.personal_link,
          twitter: decoded.twitter,
          work: decoded.work,
          verified: decoded.verified,
          createdAt: decoded.createdAt,
        })
        this.fetch_bookmarks(decoded.id)
      }
    }
  render() {
    const token = localStorage.user_token
    let user = null;
    if(token !== undefined){
      const decoded = jwt_decode(token)
       user = decoded.id;
    }else{
      user = null;
    }
    let bk = null
    if(this.state.bookmarks.length != 0){
      bk =  this.state.bookmarks;
    }
    return (
      <div>
      
        
        {token === undefined? <FOUR0FOUR/> : 
          <div className="user">
            <Navbar p={this.props}/>
            <Jumbotron className="user__jumbo">
            {/* <div style={{width:"300px"}}>
            <Particles className="particles-js-login"/>
            </div> */}
            <div style={{display:"flex", justifyContent:"flex-end"}} className="user__logout">
              <a href={`/logout`} style={{margin:"10px", float:"right"}}><Badge pill variant="danger" style={{padding:"5px"}} onClick={this.log_out.bind(this)}>Logout</Badge></a>
            </div>
          <div className="user__main">
            <div className="user__user">
                <a href="/">
                    <Image
                    style={{margin:"auto"}}
                    src={this.state.thumbnail == null?"https://www.w3schools.com/howto/img_avatar.png": this.state.thumbnail }
                    width="234px"
                    height="234px"
                    alt={this.state.name}
                    className="logoone"
                    rounded = "true"
                    />
                </a>
                <div>
                    <p style={{marginTop:"60px",marginLeft:"15px"}}> <span style={{color:"#f2f2f2",fontFamily:"charter"}}>Joined : <Moment format="D MMM YYYY" withTitle date={this.state.createdAt} /></span></p>
                    {this.state.country == null? <p></p>:<p style={{marginTop:"10px",marginLeft:"15px",fontFamily:"charter", color:"#f2f2f2"}}>Location : {this.state.country}</p>}
                    <a href={`/edit-profile`} style={{margin:"10px", float:"left"}}><Badge pill style={{padding:"5px", width:"60px", backgroundColor:"#F2AA4CFF", color:"black"}} onClick={this.handleShow}>EDIT</Badge></a>
                </div>
            </div>
            <div>
                <p style={{marginTop:"20px",marginLeft:"30px", fontSize:"30px",color:"#f2f2f2", letterSpacing:"0.10em",fontFamily:"charter"}}>{this.state.name}</p>
                <p style={{marginTop:"10px",marginLeft:"30px", fontSize:"20px",color:"#f2f2f2", letterSpacing:"0.05em",fontFamily:"charter"}}>@{this.state.username}</p>
                <p style={{marginTop:"10px",marginLeft:"30px", fontSize:"20px",color:"#f2f2f2", letterSpacing:"0.05em",fontFamily:"charter"}}>Email : {this.state.email_id}</p>
                <p style={{marginTop:"10px",marginLeft:"30px", fontSize:"20px",color:"#f2f2f2", letterSpacing:"0.05em",fontFamily:"charter"}}>About : {this.state.bio == null? "____":this.state.bio}</p>
                <p style={{marginTop:"10px",marginLeft:"30px", fontSize:"20px",color:"#f2f2f2", letterSpacing:"0.05em",fontFamily:"charter"}}>Work : {this.state.work == null? "____":this.state.work}</p>
                <div style={{marginTop:"20px",marginLeft:"30px"}}>
                  <IconContext.Provider value={{ color: "#f2f2f2" ,size:"20px", className: "global-class-name" }}>
                    <a href={this.state.github == null? '':'https://github.com/'+ this.state.github} rel="noopener noreferrer" target="_blank" style={{marginRight:"10px"}}><GoMarkGithub/></a>
                  </IconContext.Provider>
                  <IconContext.Provider value={{ color: "#f2f2f2" ,size:"20px", className: "global-class-name" }}>
                    <a href={this.state.linkedin == null? '':this.state.linkedin} style={{marginRight:"10px"}} rel="noopener noreferrer" target="_blank"><FaLinkedin/></a>
                  </IconContext.Provider>
                  <IconContext.Provider value={{ color: "#f2f2f2" ,size:"20px", className: "global-class-name" }}>
                    <a href={this.state.twitter == null? '':'https://twitter.com/'+ this.state.twitter} style={{marginRight:"10px"}} target="_blank"><FaTwitter/></a>
                  </IconContext.Provider>
                  <IconContext.Provider value={{ color: "#f2f2f2" ,size:"20px", className: "global-class-name" }}>
                    <a href={this.state.personal_link == null? '':this.state.personal_link} style={{marginRight:"10px"}} rel="noopener noreferrer" target="_blank"><FaLink/></a>
                  </IconContext.Provider>
                </div>
            </div>
          </div>
          </Jumbotron >
          {this.state.bookmarks.length == 0? <div style={{marginBottom:"200px"}} ></div>: <USERBOOKMARK p={user} post_bookmark = {(e)=>{
           
           const newUser = {
             userId: this.state.userId,
             postId: e.id
           }        
           post_bookmark(newUser)
             .then((res) => {
              this.fetch_bookmarks(user)
             }
             )
             .catch((e)=>{
               console.log(e);
             })
         
       }}  bookmark = {bk}/> }
          
        </div>
        }
        <FOOTER/>
      </div>
      
    );
  }
}

export default USERPROFILE;
