import React, { Component } from "react";
import {
    NavDropdown
  } from "react-bootstrap";

import jwt_decode from 'jwt-decode'
import ReactGA from 'react-ga';
class USERLINK extends Component {
  constructor(props) {
    super(props);
    this.state = {
        name: '',
        email_id: '',
        password: '',
        country: '',
        username: ''
    };
  }
componentDidMount() {
    const token = localStorage.user_token
    const decoded = jwt_decode(token)
    this.setState({
      name: decoded.name,
      email_id: decoded.email_id
    })
}
    log_out(e) {
        e.preventDefault()
        localStorage.removeItem('user_token')
        this.props.p.history.push(`/`)
      }
    
  render() {
    ReactGA.initialize('UA-162024881-1');
    ReactGA.set({
      name:  this.state.name =! '' ? this.state.name : "Anonymous",
      email: this.state.email_id =! ''  ? this.state.email_id : "Anonymous",
    })
    return (
        <div style={{backgroundColor:"#F2AA4CFF"}}>
            <NavDropdown drop="left" style={{letterSpacing:"0.08em",fontSize:"18px",fontFamily:"charter",color:"black"}} title={this.state.name} id="basic-nav-dropdown">
              <NavDropdown.Item href="/userprofile" style={{letterSpacing:"0.08em",fontSize:"18px",fontFamily:"charter",color:"#17252A"}}>Profile</NavDropdown.Item>
              <NavDropdown.Item href="/userprofile" style={{letterSpacing:"0.08em",fontSize:"18px",fontFamily:"charter",color:"#17252A"}}>Bookmarks</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item onClick={this.log_out.bind(this)} style={{letterSpacing:"0.08em",fontSize:"18px",fontFamily:"charter",color:"#17252A"}}>Log out</NavDropdown.Item>
            </NavDropdown>
        </div>
    );
  }
}

export default USERLINK;




