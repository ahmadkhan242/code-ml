import React, { Component } from "react";
import {
    Row,
    Col,
    Button,
    Form,
    Image
  } from "react-bootstrap";
  import "../../style/main.scss"
  import logo from '../../utils/Brainlogo3.svg'
  import { edit } from '../funtion/user_funtion'
  import jwt_decode from 'jwt-decode'
  import Swal from 'sweetalert2'
  import FOUR0FOUR from "../layout/404";

class EDITPROFILE extends Component {
  constructor() {
    super()
    this.state = {
      name: '',
      email_id: '',
      country: '',
      username: '',
      thumbnail: '',
      bio: '',
      github: null,
      linkedin: null,
      personal_link: null,
      twitter: null,
      work: null,
      createdAt:"",
      show:false,
      userId: ""
    }

    this.onChange = this.onChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value })
  }
  onSubmit(e) {
    e.preventDefault()
    const newUser = {
      name: this.state.name,
      username: this.state.username,
      email_id: this.state.email_id,
      personal_link: this.state.personal_link,
      country: this.state.country,
      twitter: this.state.twitter,
      bio: this.state.bio,
      github: this.state.github,
      linkedin: this.state.linkedin,
      work: this.state.work,
      userId: this.state.userId
    }
 
    edit(newUser).then(res => {
      if (res) {
        if(res.message == 'Success'){
          this.props.history.push(`/userprofile`)
        }
        Swal.fire({
            title: 'Sweet!',
            text: res.message,
            timer: 3000
          })
    }
      
    })
  }
  componentDidMount() {
    const token = localStorage.user_token
    
    if(token){
      const decoded = jwt_decode(token)
    this.setState({
      userId: decoded.id,
      name: decoded.name,
      email_id: decoded.email_id,
      country: decoded.country,
      username: decoded.username,
      thumbnail: decoded.thumbnail,
      bio: decoded.bio,
      github: decoded.github,
      linkedin: decoded.linkedin,
      personal_link: decoded.personal_link,
      twitter: decoded.twitter,
      work: decoded.work,
      verified: decoded.verified,
      createdAt: decoded.createdAt,
    })
  }
}
  render() {
    return (
      <div className="edit">
          <Row className="edit__row">
              <Col xs={12} md={4} className="edit__col1">
                    <div className="edit__col1__1">
                    <div style={{width:"234px", margin:"auto"}}>
                        <a href="/">
                            <Image
                            // style={{margin:"auto"}}
                            src={logo}
                            width="234px"
                            height="234px"
                            className="logoone"
                            rounded = "true"
                            />
                        </a>
                    </div>
                    <div className="edit__col1__2">
                        <p style={{textAlign:"center",fontSize:"60px"}}>
                        <span style={{color:"#F2AA4CFF",fontWeight:"bold",fontFamily:"Haas Grot Text R Web"}}>Welcome</span>
                        </p>
                    </div>
                    </div>
              </Col>

              {this.state.userId == ""? <FOUR0FOUR/>:
              <Col xs={12} md={8} className="edit__col2">
                  <div className="edit__col2__1">
                    <div id="gSignInWrapper" className="edit__col2__title">
                        <div style={{ width:"100%"}}> 
                            <p style={{color:"#F2AA4CFF", fontWeight:"bold",fontFamily:"Haas Grot Text R Web"}}>Edit your profile here.</p>
                        </div>
                    </div>
                    <hr className="edit__col2__hr"/>
                    <div className="edit__col2__form">
                    <Form onSubmit={this.onSubmit}>
                      <Form.Row>
                        <Form.Group as={Col} md="4" controlId="validationCustom01">
                          <Form.Label style={{color:"#17252A",fontFamily:"Haas Grot Text R Web"}}>Name *</Form.Label>
                          <Form.Control
                            name="first_name"
                            required
                            value={this.state.name}
                            onChange={this.onChange}
                            type="text"
                            style={{color:"#17252A",fontFamily:"Haas Grot Text R Web"}}
                            placeholder="First name"
                          />
                          <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group as={Col} md="4" controlId="validationCustom02">
                          <Form.Label style={{color:"#17252A",fontFamily:"Haas Grot Text R Web"}}>Username *</Form.Label>
                          <Form.Control
                            required
                            name="username"
                            value={this.state.username}
                            onChange={this.onChange}
                            type="text"
                            style={{color:"#17252A",fontFamily:"Haas Grot Text R Web"}}
                            placeholder="username"
                          />
                          <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group as={Col} md="4" controlId="validationCustom03">
                              <Form.Label style={{color:"#17252A",fontFamily:"Haas Grot Text R Web"}}>Country *</Form.Label>
                              <Form.Control style={{color:"#17252A",fontFamily:"Haas Grot Text R Web"}} value={this.state.country} onChange={this.onChange} name="country" type="text" placeholder="Country" required />
                              <Form.Control.Feedback type="invalid">
                                  Please provide a valid Country.
                              </Form.Control.Feedback>
                          </Form.Group>
                      </Form.Row>
                      <Form.Row>
                          <Form.Group as={Col} md="6" controlId="validationCustom03">
                              <Form.Label style={{color:"#17252A",fontFamily:"Haas Grot Text R Web"}}>Email *</Form.Label>
                              <Form.Control style={{color:"#17252A",fontFamily:"Haas Grot Text R Web"}} name="email_id"  value={this.state.email_id} onChange={this.onChange} type="email" placeholder="Email" required />
                              <Form.Control.Feedback type="invalid">
                                  Please provide a valid Email.
                              </Form.Control.Feedback>
                          </Form.Group>
                          <Form.Group as={Col} md="6" controlId="validationCustom03">
                              <Form.Label style={{color:"#17252A",fontFamily:"Haas Grot Text R Web"}}>Work</Form.Label>
                              <Form.Control style={{color:"#17252A",fontFamily:"Haas Grot Text R Web"}} value={this.state.work} onChange={this.onChange} name="work" type="text" placeholder="Work"  />
                              <Form.Control.Feedback type="invalid">
                                  Please provide a valid Work.
                              </Form.Control.Feedback>
                            </Form.Group>
                          <Form.Row>
                          
                        </Form.Row>
                      </Form.Row>
                      <Form.Row>
                          <Form.Group as={Col} md="12" controlId="validationCustom03">
                              <Form.Label style={{color:"#17252A",fontFamily:"Haas Grot Text R Web"}}>Bio *</Form.Label>
                              <Form.Control style={{color:"#17252A",fontFamily:"Haas Grot Text R Web"}} value={this.state.bio} onChange={this.onChange} name="bio" type="text" placeholder="Bio" required />
                              <Form.Control.Feedback type="invalid">
                                  Please provide a valid Bio.
                              </Form.Control.Feedback>
                          </Form.Group>
                      </Form.Row>
                      <Form.Row>
                          <Form.Group as={Col} md="6" controlId="validationCustom03">
                              <Form.Label style={{color:"#17252A",fontFamily:"Haas Grot Text R Web"}}>Github</Form.Label>
                              <Form.Control style={{color:"#17252A",fontFamily:"Haas Grot Text R Web"}} name="github"  value={this.state.github} onChange={this.onChange} type="text" placeholder="Github Username" />
                              <Form.Control.Feedback type="invalid">
                                  Please provide a valid Github.
                              </Form.Control.Feedback>
                          </Form.Group>
                          <Form.Group as={Col} md="6" controlId="validationCustom03">
                              <Form.Label style={{color:"#17252A",fontFamily:"Haas Grot Text R Web"}}>Linkedin</Form.Label>
                              <Form.Control style={{color:"#17252A",fontFamily:"Haas Grot Text R Web"}} value={this.state.linkedin} onChange={this.onChange} name="linkedin" type="text" placeholder="Linkedin Profile Link" />
                              <Form.Control.Feedback type="invalid">
                                  Please provide a valid Linkedin.
                              </Form.Control.Feedback>
                          </Form.Group>
                      </Form.Row>
                      <Form.Row>
                          <Form.Group as={Col} md="6" controlId="validationCustom03">
                              <Form.Label style={{color:"#17252A",fontFamily:"Haas Grot Text R Web"}}>Personal Link</Form.Label>
                              <Form.Control style={{color:"#17252A",fontFamily:"Haas Grot Text R Web"}} value={this.state.personal_link} onChange={this.onChange} name="personal_link" type="text" placeholder="Personal Link"  />
                              <Form.Control.Feedback type="invalid">
                                  Please provide a valid Personal Link.
                              </Form.Control.Feedback>
                          </Form.Group>
                          <Form.Group as={Col} md="6" controlId="validationCustom03">
                              <Form.Label style={{color:"#17252A",fontFamily:"Haas Grot Text R Web"}}>Twitter</Form.Label>
                              <Form.Control style={{color:"#17252A",fontFamily:"Haas Grot Text R Web"}} value={this.state.twitter} onChange={this.onChange} name="twitter"  type="text" placeholder="Twitter Username"  />
                              <Form.Control.Feedback type="invalid">
                                  Please provide a valid Twitter.
                              </Form.Control.Feedback>
                          </Form.Group>
                      </Form.Row>
                      <Button variant="primary" type="submit" style={{marginTop:"5px", backgroundColor:"#F2AA4CFF", border:"0", color:"#17252A",fontFamily:"Haas Grot Text R Web"}}>
                          Submit
                      </Button>
                    </Form>
                    </div>
                  </div>
              </Col>}
          </Row>
      </div>
    );
  }
}

export default EDITPROFILE;




