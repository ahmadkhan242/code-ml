import React, { Component } from "react";
import GoogleLogin from 'react-google-login';
import FacebookLogin from 'react-facebook-login';
import {TiSocialFacebookCircular} from 'react-icons/ti';

import {
    Nav,
    Image,
    Modal 
  } from "react-bootstrap";

import { facebook_register, google_register } from '../funtion/user_funtion'
import logo from '../../utils/Brainlogo3.svg'
import Swal from 'sweetalert2'
import ReactGA from 'react-ga';
class SIGNIN extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show:false,
            first_name: '',
            second_name: '',
            email_id: '',
            country: '',
            username: '',
            thumbnail: '',
            bio: '',
            github: null,
            linkedin: null,
            personal_link: null,
            twitter: null,
            work: null,
            createdAt:"",
            userId: ""
        };
    // this.onChange = this.onChange.bind(this)
    // this.onSubmit = this.onSubmit.bind(this)
        this.responseGoogle = this.responseGoogle.bind(this);
        this.responseFacebook = this.responseFacebook.bind(this);
      }
    handleClose = () => this.setState({ show: false });
    handleShow = () => {
        this.setState({ show: true });
        ReactGA.initialize('UA-162024881-1');
        ReactGA.event({
            category: "Login",
            action: "User pressed the login button",
          });
    }
     responseFacebook = (response) => {
         console.log(response);
        const user={
            name: response.name,
            thumbnail: response.picture.data.url,
            facebookId: response.userID,
            email_id: response.email
        }
        
        facebook_register(user).then(res => {
            if (res) {
                this.handleClose();
                if(res.logic === 'present'){
                    this.props.p.history.push(`/`)
                }else{
                    this.props.p.history.push(`/edit-profile`)
                }
                Swal.fire({
                    title: 'Sweet!',
                    text: 'Login Successfully :)',
                    imageUrl: 'https://unsplash.it/400/200',
                    imageWidth: 400,
                    imageHeight: 200,
                    imageAlt: 'Custom image',
                    timer: 3000
                  })
            }
        })
        
      }

    authFailure(res){
        console.log("SOMETHING FAILS", res);
    }
    responseGoogle(response) {
        
        const user={
            name: response.profileObj.name,
            thumbnail: response.profileObj.imageUrl,
            googleId: response.profileObj.googleId,
            email_id: response.profileObj.email
        }
        
        google_register(user).then(res => {
            if (res) {
                this.handleClose();
                if(res.logic === 'present'){
                    this.props.p.history.push(`/`)
                }else{
                    this.props.p.history.push(`/edit-profile`)
                }
                Swal.fire({
                    title: 'Sweet!',
                    text: 'Login Successfully :)',
                    imageUrl: 'https://unsplash.it/400/200',
                    imageWidth: 400,
                    imageHeight: 200,
                    imageAlt: 'Custom image',
                    timer: 3000
                  })
            }
        })
    }

  render() {
    return (
        <div>
        <Modal 
            show={this.state.show} 
            onHide={this.handleClose} 
            animation={false}
            size="sm"
            className="login"
            dialogClassName="modal-90w"
            aria-labelledby="example-custom-modal-styling-title">
            <Modal.Header closeButton style={{backgroundColor:"#101820FF", border:"0"}}>
            <Image
                alt=""
                src={logo}
                width="60px"
                height="60px"
                className="logoone"
                rounded = "true"
            />
        <p  className="auth_logo_name" >Code-ML</p>
        </Modal.Header>
            <Modal.Body style={{backgroundColor:"#101820FF",border:"0"}} >
                <div >
                    <div style={{margin:"auto"}}>
                    <p style={{textAlign:"center",fontWeight:"charter",fontSize:"40px", letterSpacing:"2px"}}><span style={{color:"#F2AA4CFF",fontFamily:"charter"}}> Log In</span></p>
                    </div>
                </div>
            <div className="auth_button">
                <div style={{margin:"10px"}}>
                    <GoogleLogin
                        clientId="587145789404-0m5tlf8sc5co28kneebk2d0jmblv7rbk.apps.googleusercontent.com"
                        // buttonText="Login"
                        onSuccess={this.responseGoogle}
                        onFailure={this.authFailure}
                        cookiePolicy={'single_host_origin'}
                        theme="dark"
                        className="my-google-button-class"
                        >
                            <span style={{marginLeft:"30px", fontFamily:"sans-serif"}}>Login with Google</span>
                    </GoogleLogin>
                </div>
                <div style={{margin:"10px"}}>
                <FacebookLogin
                    appId="592329467978746"
                    // autoLoad={true}
                    fields="name,email,picture"
                    scope="email,user_birthday,user_location,user_hometown"
                    onClick={this.componentClicked}
                    callback={this.responseFacebook}
                     cssClass="my-facebook-button-class"
                    //  textButton	="Facebook"
                    icon={<TiSocialFacebookCircular />}
                    >
                        <span>Facebook</span>
                    </FacebookLogin>
                </div>
            </div>
            </Modal.Body>
        </Modal>
        <div style={{backgroundColor:"#F2AA4CFF"}}>
            <Nav.Link style={{letterSpacing:"0.08em",fontSize:"18px",fontFamily:"charter",color:"#17252A"}} onClick={this.handleShow}>Login</Nav.Link>
        </div>
    </div>
    );
  }
}

export default SIGNIN;




