import axios from 'axios'
import {url} from "../../utils/config"

export const edit = user => {
  return axios
    .post(url+'/edit-profile', {
      name: user.name,
      email_id: user.email_id,
      country: user.country,
      username: user.username,
      bio: user.bio,
      github: user.github,
      linkedin: user.linkedin,
      password: user.password,
      personal_link: user.personal_link,
      work: user.work,
      twitter: user.twitter,
      userId: user.userId
    })
    .then(response => {
      if(response.data.data != null){
        localStorage.setItem('user_token', response.data.data)
      }
      return response.data;
    })
}

export const google_register = user => {
  return axios
    .post(url+'/google', {
      name: user.name,
      thumbnail: user.thumbnail,
      googleId: user.googleId,
      email_id: user.email_id
    })
    .then(response => {
      if(response.data.data != null){
        localStorage.setItem('user_token', response.data.data)
      }
      return response.data;
    }).catch((er) =>{
      console.log("Erorr", er);
    })
}

export const facebook_register = user => {
  return axios
    .post(url+'/facebook', {
      name: user.name,
      thumbnail: user.thumbnail,
      facebookId: user.facebookId,
      email_id: user.email_id
    })
    .then(response => {
      if(response.data.data != null){
        localStorage.setItem('user_token', response.data.data)
      }
      return response.data;
    }).catch((er) =>{
      console.log("Erorr", er);
    })
}

export const get_user_profile = user => {
  return axios
    .post(url+'/userprofile', {
      id: user.userId
    })
    .then(response => {
      return response.data.data
    })
    .catch(err => {
      console.log(err)
    })
}

export const login = user => {
  return axios
    .post(url+'/login', {
      email_id: user.email_id,
      password: user.password
    })
    .then(response => {
      if(response.data.data != null){
        localStorage.setItem('user_token', response.data.data)
      }
      return response.data.data
    })
    .catch(err => {
      console.log(err)
    })
}

export const edit_user = user => {
  return axios
    .post(url+'/edit-profile', {
      name: user.name,
      email_id: user.email_id,
      country: user.country,
      username: user.username,
      bio: user.bio,
      work: user.work,
      github: user.github,
      linkedin: user.linkedin,
      personal_link: user.personal_link,
      twitter: user.twitter
    })
    .then(response => {
      localStorage.setItem('user_token', response.data.data)
      return response.data.data
    })
    .catch(err => {
      console.log(err)
    })
}