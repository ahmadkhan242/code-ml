import axios from 'axios'
import {url} from "../../utils/config"

export const post_bookmark = newUser => {
  return axios
    .post(url+'/bookmark', {
      userId: newUser.userId,
      postId: newUser.postId
    })
    .then(response => {
      return response;
    }).catch((er) =>{
      console.log("Erorr", er);
    })
}
