import axios from 'axios'
import {url} from "../../utils/config"

export const post_comment_text = newUser => {
  return axios
    .post(url+'/comment', {
      text: newUser.text,
      userId: newUser.userId,
      postId: newUser.postId
    })
    .then(response => {
      //console.log('Comment added')
    }).catch((er) =>{
      console.log("Erorr", er);
    })
}

export const post_reply_comment_text = user => {
  return axios
    .post(url+'/reply-comment', {
        text: user.text,
        userId: user.userId,
        commentId: user.commentId,
    })
    .then(response => {
    })
    .catch(err => {
      console.log(err)
    })
}
export const get_comment = user => {
    return axios
      .get(url + user.search)
      .then(response => {
          return response.data[0].comments
      })
      .catch(err => {
        console.log(err)
      })
  }
export const get_reply_comment = user => {
return axios
    .get(url+"/reply-commentById/" + user.id)
    .then(response => {
        return response.data.data
    })
    .catch(err => {
    console.log(err)
    })
}