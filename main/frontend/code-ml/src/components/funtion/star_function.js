import axios from 'axios'
import {url} from "../../utils/config"

export const post_star = newUser => {
  return axios
    .post(url+'/post-star', {
      userId: newUser.userId==('' || undefined)?null:newUser.userId,
      postId: newUser.postId
    })
    .then(response => {
      //console.log('post-star added')
    }).catch((er) =>{
      console.log("Erorr", er);
    })
}

export const get_post_star = user => {
  return axios
    .get(url+'/post-starById/' + user.search)
    .then(response => {
        return response.data.data;
    })
    .catch(err => {
      console.log(err)
    })
}

export const comment_star = newUser => {
  return axios
    .post(url+'/comment-star', {
      userId: newUser.userId==''?null:newUser.userId,
      commentId: newUser.commentId,
      reply_commentId : newUser.reply_commentId == ('' || null)? null : newUser.reply_commentId 
    })
    .then(response => {
      //console.log('comment-star added')
    })
}

export const get_comment_star = user => {    
  return axios
    .get(url+'/comment-starById/' + user.search)
    .then(response => {
        return response.data.data;
    })
    .catch(err => {
      console.log(err)
    })
}