import React, { Component, lazy, Suspense } from "react";
import Navbar from "../layout/nav_bar";
import {Helmet} from "react-helmet";
import logo from '../../utils/Brainlogo3.svg'
const CONTENT = lazy(() => import("./content"))

class NavbarComponent extends Component {

  render() {
    return (
      <div >
        <Navbar p={this.props}/>
        <Helmet>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
            <meta name="description" content="A Machine Learning Blog for Ebullient Learner."/>
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <title>Code-ML</title>
            <link rel="shortcut icon" href={logo}/>
            <link rel="canonical" href={window.location.href} />
            
            <meta property="og:description" content="A Machine Learning Blog for Ebullient Learner. :)"/>
            <link rel="preconnect" href="https://www.w3schools.com" />
            <link rel="preconnect" href="https://www.google-analytics.com" />
            <link rel="preconnect" href="http://localhost:4000/post" />
        </Helmet>
        <div  style={{marginTop:"80px", width:"100%"}}>
          <Suspense fallback={<div className="loading"> <div className="line"></div></div>}>
          <CONTENT></CONTENT>
          </Suspense>
          
         
        </div>
        
      </div>
    );
  }
}

export default NavbarComponent;
