import React, { Component } from "react";
import {
    Row,
    Col,
    Card,
    Badge,
    Image,
    InputGroup,
    FormControl,
    Container
  } from "react-bootstrap";
  import Moment from 'react-moment';
import {  MdInsertComment} from 'react-icons/md';
import {  GiBookmark} from 'react-icons/gi';
import {  TiLightbulb} from 'react-icons/ti';
import { IconContext } from "react-icons";
import { post_bookmark } from '../funtion/bookmark_funtion'
import jwt_decode from 'jwt-decode'
import "../../style/main.scss"
import Particles from 'react-particles-js';
import FOOTER from "../layout/footer";
import Swal from 'sweetalert2'
import axios from 'axios'
import {url} from "../../utils/config"
class CONTENT extends Component {
  constructor() {
    super();
    this.state = {
      posts: [],
      postId:"",
      search: "",
      name: '',
      email_id: '',
      userId: null,
    };
  }

  post_bookmark(e){
    if(this.state.userId==null){
      Swal.fire({
        icon: 'error',
        title: 'Oops... ',
        text: 'Login to bookmark !',
        timer: 3000
      })
    }else{const newUser = {
      userId: this.state.userId,
      postId: e
    }
    post_bookmark(newUser)
      .then((res) => {
        this.fetch_post()
      }
      ).catch((er) =>{
        console.log("Erorr", er);
      })
    }
    
  }
  updateSearch(event) {
    this.setState({ search: event.target.value.substr(0, 20) });
  }
  fetch_post(){
    fetch(url+"/post")
      .then(res => res.json())
      .then(posts =>
      this.setState( {posts} , () =>
          console.log("posts fetched...")
      )
      ).catch((er) =>{
        console.log("Erorr", er);
      })
  }
  resiz(img){
    var re = img.split('&');
    var add = "&fit=crop&w=410&q=20";
    return re[0] + add;
   
  }

  componentDidMount() {
    const token = localStorage.user_token
    if(localStorage.user_token != null){
      this.setState({
        userId: jwt_decode(localStorage.user_token).id,
        name: jwt_decode(localStorage.user_token).name,
        email_id: jwt_decode(localStorage.user_token).email_id
      })
    }
    this.fetch_post()

    }

  render() {
    let filters;
    if(this.state.posts.length == 0 || this.state.posts.length == undefined ){
        filters = [];
    }else{
      filters = this.state.posts.filter(visitor => {
        return (visitor.title.replace(/[-]/g, " ").toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1)
      });
    }
    return (
        <div className="content">
          <div className="content_intro" style={{background:"#191b1f"}}>
             <Particles className="particles-js"/>
          <Row style={{width:"100%", margin:"0"}}>
            <Col xs={1} md={3}></Col>
            <Col xs={10} md={6}>
              <InputGroup style={{width:"100%", marginTop:"40px", marginBottom:"10px"}}>
                    <InputGroup.Prepend>
                      <InputGroup.Text id="inputGroup-sizing-default" style={{backgroundColor:"#F2AA4CFF", border:"0", color:"black"}}>
                        Search 
                      </InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl
                      value={this.state.search}
                      onChange={this.updateSearch.bind(this)}
                      placeholder="try linear regression..."
                      aria-label="Default"
                      aria-describedby="inputGroup-sizing-default"
                    />
                  </InputGroup>
            </Col>
            <Col xs={1} md={3}></Col>
          </Row>
          <Row style={{width:"100%", margin:"0"}}>
          <Container>
            <Row >
              <Col xs={0} md={2}><span ><a style={{color:"#F2AA4CFF", margin:"5px"}} aria-label="tag" ><Badge pill style={{backgroundColor:"black", padding:"10px"}}></Badge></a></span></Col>
              <Col xs={6} md={2} className="text-center"  style={{padding:"10px"}}><span ><a  aria-label="tag" style={{color:"#F2AA4CFF", margin:"5px", letterSpacing:"0.05em"}}><Badge pill style={{backgroundColor:"black", padding:"10px", boxShadow:"1px 2px #F2AA4CFF"}}><span style={{fontSize:"15px", fontWeight:"bold"}}>#</span>MachineLearning</Badge></a></span></Col>
              <Col xs={6} md={2} className="text-center" style={{padding:"10px"}}><span ><a aria-label="tag" style={{color:"#F2AA4CFF", margin:"5px", letterSpacing:"0.05em"}}><Badge pill style={{backgroundColor:"black", padding:"10px", boxShadow:"1px 2px #F2AA4CFF"}}><span style={{fontSize:"15px", fontWeight:"bold"}}>#</span>NeuralNetwork</Badge></a></span></Col>
              <Col xs={6} md={2} className="text-center" style={{padding:"10px"}}><span ><a aria-label="tag" style={{color:"#F2AA4CFF", margin:"5px", letterSpacing:"0.05em"}}><Badge pill style={{backgroundColor:"black", padding:"10px", boxShadow:"1px 2px #F2AA4CFF"}}><span style={{fontSize:"15px", fontWeight:"bold"}}>#</span>Regression</Badge></a></span></Col>
              <Col xs={6} md={2} className="text-center" style={{padding:"10px"}}><span ><a aria-label="tag" style={{color:"#F2AA4CFF", margin:"5px", letterSpacing:"0.05em"}}><Badge pill style={{backgroundColor:"black", padding:"10px", boxShadow:"1px 2px #F2AA4CFF"}}><span style={{fontSize:"15px", fontWeight:"bold"}}>#</span>DeepLearning</Badge></a></span></Col>
            </Row>
            <Row style={{marginTop:"1px"}}>
              <Col xs={6} md={3} className="text-center" style={{padding:"10px"}}><Badge pill style={{backgroundColor:"black", padding:"10px", float:"center", boxShadow:"1px 2px #F2AA4CFF"}}><a aria-label="tag" style={{color:"#F2AA4CFF", margin:"5px", letterSpacing:"0.05em"}}><span style={{fontSize:"15px", fontWeight:"bold"}}>#</span>OpenCv</a></Badge></Col>
              <Col xs={6} md={3} className="text-center" style={{padding:"10px"}}><span style={{alignSelf:"center"}}><a  aria-label="tag" style={{color:"#F2AA4CFF", margin:"5px", letterSpacing:"0.05em"}}><Badge pill style={{backgroundColor:"black", padding:"10px", boxShadow:"1px 2px #F2AA4CFF"}}><span style={{fontSize:"15px", fontWeight:"bold"}}>#</span>Example</Badge></a></span></Col>
              <Col xs={6} md={3} className="text-center" style={{padding:"10px"}}><span ><a aria-label="tag" style={{color:"#F2AA4CFF", margin:"5px", letterSpacing:"0.05em"}}><Badge pill style={{backgroundColor:"black", padding:"10px", boxShadow:"1px 2px #F2AA4CFF"}}><span style={{fontSize:"15px", fontWeight:"bold"}}>#</span>optimisation</Badge></a></span></Col>
              <Col xs={6} md={2} className="text-center" style={{padding:"10px"}}><span ><a aria-label="tag" style={{color:"#F2AA4CFF", margin:"5px", letterSpacing:"0.05em"}}><Badge pill style={{backgroundColor:"black", padding:"10px", boxShadow:"1px 2px #F2AA4CFF"}}><span style={{fontSize:"15px", fontWeight:"bold"}}>#</span>googleColab</Badge></a></span></Col>
            </Row>
            <Row style={{marginTop:"1px"}}>
              <Col xs={0} md={2}><span ><a style={{color:"#F2AA4CFF", margin:"5px"}} aria-label="tag"><Badge pill style={{backgroundColor:"black", padding:"10px"}}></Badge></a></span></Col>
              <Col xs={6} md={2} className="text-center" style={{padding:"10px"}}><span ><a aria-label="tag" style={{color:"#F2AA4CFF", margin:"5px", letterSpacing:"0.05em"}}><Badge pill style={{backgroundColor:"black", padding:"10px", boxShadow:"1px 2px #F2AA4CFF"}}><span style={{fontSize:"15px", fontWeight:"bold"}}>#</span>tensorflow</Badge></a></span></Col>
              <Col xs={6} md={2} className="text-center" style={{padding:"10px"}}><span ><a aria-label="tag" style={{color:"#F2AA4CFF", margin:"5px", letterSpacing:"0.05em"}}><Badge pill style={{backgroundColor:"black", padding:"10px" , boxShadow:"1px 2px #F2AA4CFF"}}><span style={{fontSize:"15px", fontWeight:"bold"}}>#</span>Polynomial</Badge></a></span></Col>
              <Col xs={6} md={3} className="text-center" style={{padding:"10px"}}><span ><a  aria-label="tag" style={{color:"#F2AA4CFF", margin:"5px", letterSpacing:"0.05em"}}><Badge pill style={{backgroundColor:"black", padding:"10px", boxShadow:"1px 2px #F2AA4CFF"}}><span style={{fontSize:"15px", fontWeight:"bold"}}>#</span>keras</Badge></a></span></Col>
              <Col xs={6} md={2} className="text-center" style={{padding:"10px"}}><span ><a aria-label="tag" style={{color:"#F2AA4CFF", margin:"5px", letterSpacing:"0.05em"}}><Badge pill style={{backgroundColor:"black", padding:"10px", boxShadow:"1px 2px #F2AA4CFF"}}><span style={{fontSize:"15px", fontWeight:"bold"}}>#</span>tech</Badge></a></span></Col>
              <Col xs={6} md={2}><span ><a style={{color:"#F2AA4CFF", margin:"5px"}} aria-label="tag"><Badge pill style={{backgroundColor:"black", padding:"10px"}}></Badge></a></span></Col>
            </Row>  
          </Container>
          </Row>
        </div>
          <div style={{ background:"#191b1f", width:"100%"}} className="article">         
            <h3 className="article__title"> Articles</h3>
            <hr className="article__hr"/>
                            {filters.length == 0 ?<div>  </div>  : filters.map((p, i) => (
                              <div key={i} className="article__cardDiv">
                                <Card className="article__card">
                                    <Card.Body className="article__cardBody">
                                      <Row className="article__cardRow" >
                                        <Col xs={12} md={4} className="article__colImg">
                                        <Image
                                          style={{marginRight:"5px"}}
                                          alt={p.title}
                                          src={this.resiz(p.cover_img)}
                                          width="100%"
                                          height="100%"
                                          className="logoone"
                                          rounded = "true"
                                        />
                                        </Col>
                                        <Col xs={12} md={8} style={{ padding:"0"}} className="article__colBd">
                                          <div style={{display:"flex", justifyContent:"start", marginTop:"10px"}} className="article__img">
                                          <Image
                                          style={{marginLeft:"0"}}
                                            alt={p.user.username}
                                            src={ p.user.thumbnail == null?"https://www.w3schools.com/howto/img_avatar.png": p.user.thumbnail }
                                            width="45px"
                                            height="45px"
                                            className="logoone"
                                            roundedCircle = "true"
                                          />  
                                          <Card.Title  className="article__cardTitle">
                                            <a style={{ textDecoration:"none"}} href={"https://code-ml.com/post/"+ p.title}><p className="article__titleText">{p.title==null?`No post Found`:p.title.replace(/[-]/g, " ")}</p></a>
                                          </Card.Title>
                                          </div>
                                          <Card.Text style={{ marginBottom:"5px"}} className="article__text">
                                              <div ><a style={{color:"#f2f2f2", textDecoration:"none"}} href={"/user/"+p.user.username} >{p.user.name+" . "}</a><span style={{color:"#f2f2f2",fontFamily:"normal"}}><Moment format="D MMM YYYY" withTitle date={p.createdAt} /></span></div>
                                              <div style={{color:"#f2f2f2",fontFamily:"montserrat"}} >
                                                tags - {p.tags.map(function(object, i){
                                                  return <span key={i} ><a href={`https://code-ml.com/tag/${object}`} style={{color:"#F2AA4CFF", margin:"5px"}}><Badge pill style={{backgroundColor:"black"}}>#{object}</Badge></a></span>;
                                                })}
                                              </div>
                                          </Card.Text>
                                          <footer className="article__cardFooter">
                                            <div>
                                              <IconContext.Provider value={{ color: "#f2f2f2",size:"20px", className: "global-class-name" }}>
                                              <TiLightbulb/>
                                              </IconContext.Provider>
                                              <span style={{paddingRight:"10px", paddingLeft:"5px", color:"#f2f2f2"}}>{p.stars == null ? 0 : p.stars}</span>
                                              <IconContext.Provider value={{ color: "#f2f2f2" ,size:"20px", className: "global-class-name" }}>
                                                <MdInsertComment/>
                                              </IconContext.Provider>
                                              <span style={{paddingRight:"10px", paddingLeft:"5px", color:"#f2f2f2"}}>{p.comment_count}</span>   
                                              <span style={{ paddingLeft:"5px", color:"#f2f2f2"}}>{p.read_time.text}</span>   
                                            </div>
                                            <div>
                                              <IconContext.Provider value={{ color: "#f2f2f2" ,size:"20px", className: "global-class-name" }}>
                                                <GiBookmark style={{cursor:"pointer"}} onClick={()=>(this.post_bookmark(p.id))}/>
                                              </IconContext.Provider>
                                              <span style={{paddingLeft:"5px", color:"#f2f2f2"}}>{p.bookmark_count}</span>
                                            </div>
                                          </footer>
                                        </Col>
                                      </Row>
                                    </Card.Body>
                                </Card>
                                <hr className="article__hr"/>
                                </div>
                            ))}
                            <FOOTER/>
                   </div>
                   
        </div>
      
    );
  }
}

export default CONTENT;
