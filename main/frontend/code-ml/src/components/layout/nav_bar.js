import React, { Component} from "react";
import "../../style/main.scss"
import {
    Navbar,
    Container,
    Nav,
    Image,
  } from "react-bootstrap";

import USERLINK from '../auth/user_link'
import logo from '../../utils/Brainlogo3.svg'
import '../../style/google_button.css'
import SIGNIN from "../auth/sign_in";

class NavbarComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
          email:"",
          password:""
        };
      }
    componentDidMount() {
        window.addEventListener("scroll", this.resizeHeaderOnScroll);
      }
      resizeHeaderOnScroll() {
        const distanceY = window.pageYOffset || document.documentElement.scrollTop,
          shrinkOn = 200,
          headerEl = document.getElementById("navnav"),
          logoC = document.getElementsByClassName("logoone");

        if (distanceY > shrinkOn) {
            headerEl.style.height = "60px"
            logoC[0].height = "45"
            logoC[0].width = "45"
        } else {
            headerEl.style.height = "80px"
            logoC[0].height = "60"
            logoC[0].width = "60"
        }
      }

  render() {
    const login_Link = (
      <SIGNIN p={this.props.p}/>
    )
    const user_link = (
      <USERLINK p={this.props.p}/>
    )
     
    return (
     <div style={{width:"100%",display:"flex", flexDirection:"row"}}>  
     <Navbar  expand="lg" fixed="top" id = "navnav" className="navbar">
            {/* <Container className="navbar__container"> */}
              <Navbar.Brand href="/" className="navbar__brand">
                  <Image
                      alt=""
                      src={logo}
                      width="60px"
                      height="60px"
                      className="logoone"
                      rounded = "true"
                  /><span className="navbar__title"> Code-ML</span>
              </Navbar.Brand>
            
            <Navbar.Toggle aria-controls="basic-navbar-nav" className="navbar__toggle"/>
              <Navbar.Collapse id="basic-navbar-nav" className="navbar__collapse justify-content-end">
                <Nav className="navbar_nav">
                    <Nav.Link href= {"https://code-ml.com/category/Machine Learning"} style={{color:"#F2AA4CFF"}}  className="navbar__nav__link">Machine Learning</Nav.Link>
                    {/* <Nav.Link href= {"https://code-ml.com/category/Deep Learning"} style={{color:"#F2AA4CFF"}} className="navbar__nav__link">Deep Learning</Nav.Link> */}
                    <Nav.Link href= {"https://code-ml.com/category/Resource"} style={{color:"#F2AA4CFF"}} className="navbar__nav__link">Resources</Nav.Link>
                    {localStorage.user_token ? user_link : login_Link}
                </Nav>
              </Navbar.Collapse>
            {/* </Container> */}
        </Navbar>
        </div>
    );
  }
}

export default NavbarComponent;
