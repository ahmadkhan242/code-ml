import React, { Component } from "react";
import {
    ListGroup,
    Card
    } from "react-bootstrap";
import Moment from 'react-moment';
import {  MdInsertComment} from 'react-icons/md';
import { GiBookmark} from 'react-icons/gi';
import { TiLightbulb} from 'react-icons/ti';
import { IconContext } from "react-icons";
import jwt_decode from 'jwt-decode'
import { get_user_profile } from '../funtion/user_funtion'
import { post_bookmark } from '../funtion/bookmark_funtion'
import Swal from 'sweetalert2'
import "../../style/main.scss"
import {url} from "../../utils/config"

class RIGHTSIDEBAR extends Component {
    constructor(props) {
        super(props);
        this.state = {
            recent_posts:[],
            posts:[],
          inputLinkClicked: false,
          comment_text:"",
          comments:[],
          postId:"",
          first_name: '',
          second_name: '',
          email_id: '',
          userId:null,
          post_star_count:"",
          bookmark: []
        };
      }
      post_bookmark(e){
        if(this.state.userId==null){
          Swal.fire({
            icon: 'error',
            title: 'Oops... ',
            text: 'Login to bookmark !',
            timer: 3000
          })
        }else{
          const newUser = {
            userId: this.state.userId,
            postId: e
          }
          post_bookmark(newUser)
            .then((res) => {
              this.fetch_bookmark(newUser.userId)
              this.fetch_recent_post()
            }
            );
        }
        
      }
      fetch_bookmark(e){
        get_user_profile({userId:e})
        .then(res => {
          this.setState({
            bookmark: res.bookmark
          })
        })
      }
      fetch_recent_post(){
        fetch(url+"/recent")
        .then(res => res.json())
        .then(recent_posts =>
        this.setState( {recent_posts}
        )
        );
      }
      fetch_post(){
        fetch(url+"/post/"+ this.props.post.title)
        .then(res => res.json())
        .then(posts =>
        this.setState( {posts} , () =>
            this.setState({postId: posts[0].post.id})
        )
        );
      }
    componentDidMount() {
        if(localStorage.user_token){
            this.setState({
              userId: jwt_decode(localStorage.user_token).id,
              first_name: jwt_decode(localStorage.user_token).first_name,
              second_name: jwt_decode(localStorage.user_token).second_name,
              email_id: jwt_decode(localStorage.user_token).email_id
            })
            this.fetch_bookmark(jwt_decode(localStorage.user_token).id)
          }
          this.fetch_recent_post();
    }

  render() {
        let recent_posts_filter;
        if(this.state.recent_posts.length == 0 || this.state.recent_posts.length == undefined ){
            recent_posts_filter = [];
        }else{
        recent_posts_filter = this.state.recent_posts.filter(visitor => {
            return (
            visitor
            );
        });
        }
    return (
        <div style={{paddingTop:"17px", marginLeft:"0px"}}>
            {(this.state.userId == null || this.state.bookmark.length == 0) ? <ListGroup.Item className="noBookmark">
            <p style={{color:"#F2AA4CFF", display:"flex", justifyContent:"center",letterSpacing:"0.2em",lineHeight:"1.4em",fontWeight:"bold",fontFamily:"charter"}}>Bookmarks</p>
            <p style={{color:"white", display:"flex", justifyContent:"center", fontSize:"15px"}}>No Bookmark</p>
            </ListGroup.Item> :
          <ListGroup.Item className="bookmarkBar">
                <p style={{color:"#F2AA4CFF", display:"flex", justifyContent:"center",letterSpacing:"0.2em",lineHeight:"1.4em",fontWeight:"bold",fontFamily:"charter"}}>Bookmarks</p>
                {this.state.bookmark.map((rp, i)=> (
                <a style={{color:"#17252A", textDecoration:"none"}} href={"https://code-ml.com/post/"+ rp.post.title}>
                    <Card className="bookmarkBar__card">
                    <Card.Body>
                        <Card.Title className="recentBar__cardTitle">{rp.post.title.replace(/[-]/g, " ")}</Card.Title>
                        <Card.Subtitle className="mb-2 text-muted">
                        <div style={{color:"white",fontFamily:"charter"}}><span style={{color:"white",fontFamily:"normal"}}><Moment format="D MMM YYYY" withTitle date={rp.post.createdAt} /></span></div>
                        </Card.Subtitle>
                        <footer className="bookmarkBar__cardFooter">
                            <div>
                                <IconContext.Provider value={{ color: "white",size:"20px", className: "global-class-name" }}>
                                    <TiLightbulb/>
                                    <span style={{color: "white",paddingRight:"10px", paddingLeft:"5px"}}>{rp.post.stars == null ? 0 : rp.post.stars}</span>
                                </IconContext.Provider>
                                <IconContext.Provider value={{ color: "white" ,size:"20px", className: "global-class-name" }}>
                                    <MdInsertComment/>
                                    <span style={{color: "white",paddingRight:"10px", paddingLeft:"5px"}}>{rp.post.comment_count}</span>
                                </IconContext.Provider>
                            </div>
                            <div>
                                <IconContext.Provider value={{ color: "white" ,size:"20px", className: "global-class-name" }}>
                                <GiBookmark style={{cursor:"pointer"}} onClick={()=>(this.state.userId==null ? alert("Login to bookmark !!"):this.post_bookmark(rp.post.id))}/>
                                </IconContext.Provider>
                                <span style={{color: "white",paddingLeft:"5px"}}>{rp.post.bookmark_count}</span>
                            </div>
                        </footer>
                    </Card.Body>
                    </Card>
                    </a>
                ))}
                </ListGroup.Item>
          }
            <ListGroup.Item className="recentBar">
                <p className="recentBar__title">Recent Posts</p>
                {recent_posts_filter.map((rp, i)=> (
                
                    <Card className="recentBar__card" >
                    <Card.Body>
                        <Card.Title style={{fontFamily:"charter"}}><a href={"https://code-ml.com/post/"+ rp.title} className="recentBar__cardTitle">{rp.title.replace(/[-]/g, " ")} </a></Card.Title>
                        <Card.Subtitle className="mb-2 text-muted">
                        <div style={{color:"#f2f2f2",fontFamily:"charter"}}>{rp.user.name+" . "}<span style={{color:"#f2f2f2",fontFamily:"normal"}}><Moment format="D MMM YYYY" withTitle date={rp.createdAt} /></span></div>
                        </Card.Subtitle>
                        <Card.Text style={{color:"#f2f2f2"}}>
                            {rp.read_time.text}
                        </Card.Text>
                        <footer className="recentBar__cardFooter">
                            <div>
                                <IconContext.Provider value={{ color: "#f2f2f2",size:"20px", className: "global-class-name" }}>
                                    <TiLightbulb/>
                                    <span style={{color: "#f2f2f2",paddingRight:"10px", paddingLeft:"5px"}}>{rp.stars == null ? 0 : rp.stars}</span>
                                </IconContext.Provider>
                                <IconContext.Provider value={{ color: "#f2f2f2",size:"20px", className: "global-class-name" }}>
                                    <MdInsertComment/>
                                    <span style={{color: "#f2f2f2",paddingRight:"10px", paddingLeft:"5px"}}>{rp.comment_count}</span>
                                </IconContext.Provider>
                            </div>
                            <div>
                                <IconContext.Provider value={{ color: "#f2f2f2",size:"20px", className: "global-class-name" }}>
                                    <GiBookmark style={{cursor:"pointer"}} onClick={()=>(this.post_bookmark(rp.id))}/>
                                    <span style={{color: "#f2f2f2",paddingLeft:"5px"}}>{rp.bookmark_count}</span>
                                </IconContext.Provider>
                            </div>
                        </footer>
                    </Card.Body>
                    </Card>
                   
                ))}
                </ListGroup.Item>
            <ListGroup/>
        </div>
    );
  }
}

export default RIGHTSIDEBAR;




