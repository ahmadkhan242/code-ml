import React, { Component } from "react";
import {
    ListGroup
  } from "react-bootstrap";
import { post_bookmark } from '../funtion/bookmark_funtion'
import jwt_decode from 'jwt-decode'
import Swal from 'sweetalert2'
import {url} from "../../utils/config"

class LEFTSTARSIDEBAR extends Component {
    constructor(props) {
        super(props);
        this.state = {
            posts:[],
          inputLinkClicked: false,
          comment_text:"",
          comments:[],
          postId:"",
          first_name: '',
          second_name: '',
          email_id: '',
          userId:null,
          post_star_count:"",
          bookmark: []
        };
        this.post_bookmark = this.post_bookmark.bind(this);
      }
      post_bookmark(e){
        if(this.state.userId==null){
          Swal.fire({
            icon: 'error',
            title: 'Oops... ',
            text: 'Login to bookmark !',
            timer: 3000
          })
        }else{
          const newUser = {
            userId: this.state.userId,
            postId: e
          }
          post_bookmark(newUser)
            .then((res) => {
              this.fetch_post()
            }
            );
        }
      }
      
      fetch_post(){
        fetch(url+"/post/"+ this.props.post.title)
        .then(res => res.json())
        .then(posts =>
        this.setState( {posts} , () =>
            this.setState({postId: posts[0].post.id})
        )
        );
      }
    componentDidMount() {
        
        if(localStorage.user_token){
            this.setState({
              userId: jwt_decode(localStorage.user_token).id,
              first_name: jwt_decode(localStorage.user_token).first_name,
              second_name: jwt_decode(localStorage.user_token).second_name,
              email_id: jwt_decode(localStorage.user_token).email_id
            })
          }
    }

  render() {
    return (
      <div >
          <ListGroup  style={{marginBottom:"5px", marginTop:"0px"}}>
                <ListGroup.Item style={{display:"flex", justifyContent:"center",letterSpacing:"0.2em",lineHeight:"1.4em",fontWeight:"bold",fontFamily:"charter", borderWidth:"0", background:"#101820FF"}}><span style={{color:"#F2AA4CFF"}}>Topics</span></ListGroup.Item>
                <ListGroup.Item style={{background:"#101820FF", borderWidth:"2px", borderColor:"black"}}><a style={{color:"#fafafa", textDecoration:"none",fontFamily:"charter"}} href="https://code-ml.com/category/Machine Learning">Machine Learning</a></ListGroup.Item>
                {/* <ListGroup.Item style={{background:"#101820FF", borderWidth:"2px", borderColor:"black"}}><a style={{color:"#fafafa", textDecoration:"none",fontFamily:"charter"}} href="https://code-ml.com/category/Deep Learning">Deep Learning</a></ListGroup.Item> */}
                <ListGroup.Item style={{background:"#101820FF", borderWidth:"2px", borderColor:"black"}}><a style={{color:"#fafafa", textDecoration:"none",fontFamily:"charter"}} href="https://code-ml.com/category/Resource">Resource</a></ListGroup.Item>
            </ListGroup>
          </div>
    );
  }
}

export default LEFTSTARSIDEBAR;




