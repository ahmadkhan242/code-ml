import React, { Component } from "react";
import {
    Button,
    Card
  } from "react-bootstrap";
  import FOOTER from "../layout/footer";
class FOUR0FOUR extends Component {
    
  render() {
    return (
        <div  className="C404">
          <Card className="text-center C404__card">
            <Card.Header style={{fontWeight:"bold", fontSize:"30px"}}>404</Card.Header>
            <Card.Body>
              <Card.Title>Page Not Found</Card.Title>
              <Button variant="primary" style={{background:"#F2AA4CFF", border:"none"}}><a href="/" style={{color:"white"}}>Go Home</a></Button>
            </Card.Body>
          </Card>
        </div>
    );
  }
}

export default FOUR0FOUR;




