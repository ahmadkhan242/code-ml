import React, { Component } from "react";
import Navbar from "../layout/nav_bar";
import Moment from 'react-moment';
import logo from '../../utils/Brainlogo3.svg'
import {url} from "../../utils/config"

import {
    Row,
    Col,
    Card,
    Badge,
    Image
  } from "react-bootstrap";
  import Swal from 'sweetalert2'
import {  MdInsertComment} from 'react-icons/md';
import {  GiBookmark} from 'react-icons/gi';
import {  TiLightbulb} from 'react-icons/ti';
import { IconContext } from "react-icons";
import RIGHTSIDEBAR from "../layout/right_sidebar"
import jwt_decode from 'jwt-decode'
import { post_bookmark } from '../funtion/bookmark_funtion'
import "../../style/main.scss"
import {Helmet} from "react-helmet";
import FOOTER from "../layout/footer";
import FOUR0FOUR from "../layout/404";

class TOPICMAIN extends Component {
    constructor() {
        super();
        this.state = {
          posts: [],
          search: "",
          show: false,
          postId:"",
          name: '',
          email_id: '',
          userId:null,
        };
      }

    post_bookmark(e){
      if(this.state.userId==null){
        Swal.fire({
          icon: 'error',
          title: 'Oops... ',
          text: 'Login to bookmark !',
          timer: 3000
        })
      }else{
        const newUser = {
          userId: this.state.userId,
          postId: e
        }
        post_bookmark(newUser)
          .then((res) => {
            this.fetch_post();
          }
          );
      }
      
    }
    fetch_post(){
      fetch(url +this.props.location.pathname+ this.props.location.search)
        .then(res => res.json())
        .then(posts =>
        this.setState( {posts} , () =>
            console.log("posts fetched...")
        )
        ).catch((er) =>{
          console.log("Erorr", er);
        })
    }
    componentDidMount() {
      if(localStorage.user_token){
        this.setState({
          userId: jwt_decode(localStorage.user_token).id,
          name: jwt_decode(localStorage.user_token).name,
          email_id: jwt_decode(localStorage.user_token).email_id
        })
      }
     this.fetch_post();
    }
  render() {
    let filters;
      if(this.state.posts.length == 0 || this.state.posts.length == undefined ){
          filters = [];
      }else{
        filters = this.state.posts.filter(visitor => {
          return (
            visitor
          );
        });
      }
      let mainURL = this.props.location.pathname.split("/");
      let mainTag = mainURL[1].toUpperCase();
      let mainQuery = mainURL[2];
    return (
      <div style={{height:"100%"}} className="main">
         <Helmet>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <meta property="og:title" content={this.props.location.pathname.replace(/['/']/g, ' ')} />
            <meta name="twitter:card" content={this.props.location.pathname.replace(/['/']/g, ' ')}/>
            <link rel="shortcut icon" href={logo}/>
            <link rel="canonical" href={window.location.href} />
            <meta name="description" content={mainTag + " - " + mainQuery}/>
            <title>{mainTag +" - " + mainQuery}</title>
        </Helmet>
            <Navbar p={this.props}/>
            <div className="main__main">
                <div style={{margin:"10px"}} className="main__post">                        
                  <h2 className="main__title"><span>{mainTag +" - " + mainQuery}</span></h2>
                  <hr className="main__hr"/>
                            {filters.length == 0 ? <FOUR0FOUR/>  : filters.map((p, i) => (
                              <div>
                                <Card className="main__card">
                                    <Card.Body className="main__cardBody">
                                      <Row className="main__cardRow">
                                        <Col  xs={12} md={4} className="main__colImg">
                                        <Image
                                          style={{marginLeft:"0"}}
                                          src={p.cover_img}
                                          width="100%"
                                          height="100%"
                                          className="logoone"
                                          rounded = "true"
                                        />
                                        </Col>
                                        <Col  xs={12} md={8} style={{ padding:"0"}} className="main__colBd">
                                          <div style={{display:"flex", justifyContent:"start", marginTop:"10px"}} className="main__img">
                                          <Image
                                          style={{marginLeft:"0"}}
                                            alt=""
                                            src={p.user.thumbnail == null?"https://www.w3schools.com/howto/img_avatar.png": p.user.thumbnail }
                                            width="45px"
                                            height="45px"
                                            className="logoone"
                                            roundedCircle = "true"
                                          />  
                                          <Card.Title className="main__cardTitle">
                                            <a style={{ textDecoration:"none"}} href={"https://code-ml.com/post/"+ p.title}><p className="main__titleText">{p.title==null?`No post Found`:p.title.replace(/[-]/g, " ")}</p></a>
                                          </Card.Title>
                                          </div>
                                          <Card.Text style={{ marginBottom:"5px"}} className="main__text">
                                              <div ><a style={{color:"#f2f2f2", textDecoration:"none"}} href={"/user/"+p.user.username} >{p.user.name+" . "}</a><span style={{color:"#f2f2f2",fontFamily:"normal"}}><Moment format="D MMM YYYY" withTitle date={p.createdAt} /></span></div>
                                              <div style={{color:"#f2f2f2",fontFamily:"montserrat"}} >
                                                tags - {p.tags.map(function(object, i){
                                                  return <span ><a href={`https://code-ml.com/tag/${object}`} style={{color:"#F2AA4CFF", margin:"5px"}}><Badge pill style={{backgroundColor:"black"}}>#{object}</Badge></a></span>;
                                                })}
                                              </div>
                                          </Card.Text>
                                          <footer className="main__cardFooter">
                                            <div>
                                              <IconContext.Provider value={{ color: "#f2f2f2",size:"20px", className: "global-class-name" }}>
                                              <TiLightbulb/>
                                              </IconContext.Provider>
                                              <span style={{color: "#f2f2f2",paddingRight:"10px", paddingLeft:"5px"}}>{p.stars}</span>
                                              <IconContext.Provider value={{ color: "#f2f2f2",size:"20px", className: "global-class-name" }}>
                                                <MdInsertComment/>
                                              </IconContext.Provider>
                                              <span style={{color: "#f2f2f2",paddingRight:"10px", paddingLeft:"5px"}}>{p.comment_count}</span>   
                                              <span style={{color: "#f2f2f2", paddingLeft:"5px"}}>{p.read_time.text}</span>   
                                            </div>
                                            <div>
                                              <IconContext.Provider value={{ color: "#f2f2f2",size:"20px", className: "global-class-name" }}>
                                                <GiBookmark style={{cursor:"pointer"}} onClick={()=>(this.post_bookmark(p.id))}/>
                                              </IconContext.Provider>
                                              <span style={{color: "#f2f2f2",paddingLeft:"5px"}}>{p.bookmark_count}</span>
                                            </div>
                                          </footer>
                                        </Col>
                                      </Row>
                                    </Card.Body>
                                </Card>
                                <hr className="main__hr"/>
                                </div>
                            ))}
                   </div>
                   <div className="main__right"> 
                   <RIGHTSIDEBAR/>                         
                   </div>
            </div>
            <FOOTER/>
      </div>
    );
  }
}

export default TOPICMAIN;
