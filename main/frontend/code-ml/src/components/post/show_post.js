import React, { Component } from "react";
import Navbar from "../layout/nav_bar";
import FOOTER from "../layout/footer";
import LEFTSTARSIDEBAR from "../layout/left_star_sidebar";
import Moment from 'react-moment';
import parse from 'html-react-parser';
import {Helmet} from "react-helmet";
import {url} from "../../utils/config"
import {
    Image,
    ListGroup,
    Form,
    Card,
    Container,
    Button,
    Badge
  } from "react-bootstrap";

  import {
    FacebookShareButton,
    FacebookIcon,
    FacebookShareCount,
    LinkedinShareButton,
    TwitterIcon,
    WhatsappIcon,
    LinkedinIcon,
    TelegramIcon,
    TelegramShareButton,
    TwitterShareButton,
    WhatsappShareButton,
  } from "react-share";

 
import "../../style/post.css"
import "../../style/main.scss"
import COMMENT from "./comment";
import { post_comment_text } from '../funtion/comment_funtion'
import logo from '../../utils/Brainlogo3.svg'
import { get_post_star, post_star } from '../funtion/star_function'
import { post_bookmark } from '../funtion/bookmark_funtion'
import jwt_decode from 'jwt-decode'
import {  GoMarkGithub} from 'react-icons/go';
import {  FaLinkedin,FaTwitter,FaLink } from 'react-icons/fa';
import { IconContext } from "react-icons";
// import 'react-s-alert/dist/s-alert-default.css';
// import 'react-s-alert/dist/s-alert-css-effects/slide.css';
import Swal from 'sweetalert2'
import { GiBookmark} from 'react-icons/gi';
import { get_user_profile } from '../funtion/user_funtion'
import {  TiLightbulb} from 'react-icons/ti';
import {  MdInsertComment} from 'react-icons/md';

class POSTMAIN extends Component {

    constructor() {
        super();
        this.state = {
          posts:[],
          recent_posts:[],
          inputLinkClicked: false,
          comment_text:"",
          comments:[],
          postId:"",
          name: '',
          email_id: '',
          userId:null,
          post_star_count:"",
          postTitle:"",
          alertVisible: false,
          bookmark: []
        }
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
      }
      onChange(e) {
        this.setState({ [e.target.name]: e.target.value })
      }
      toggle(){
        this.setState({alertVisible: !this.state.alertVisible})
      }
      fetch_bookmark(e){
        get_user_profile({userId:e})
        .then(res => {
          this.setState({
            bookmark: res.bookmark
          })
        })
        .catch((er) =>{
          console.log("Erorr", er);
        })
      }
      onSubmit(e) {
        e.preventDefault()
        if(this.state.userId == null ){
          Swal.fire({
            title: 'Sorry!',
            text: 'Login first :)',
            icon: 'error',
            timer: 3000
          })
        }else{
          if(this.state.comment_text==""){
            Swal.fire({
              text: 'Nothing to comment',
              icon: 'error',
              timer: 3000
            })
          }else{
            const newUser = {
              text: this.state.comment_text,
              postId: this.state.postId,
              userId: this.state.userId
            }
            post_comment_text(newUser).then(res => {
              this.setState({comment_text:""})
              this.fetch_post();
            }).catch((er) =>{
              console.log("Erorr", er);
            })
          }
          }
      }
      post_bookmark(e){
        if(this.state.userId==null){
          Swal.fire({
            icon: 'error',
            title: 'Oops... ',
            text: 'Login to bookmark !',
            timer: 3000
          })
        }else{
          const newUser = {
            userId: this.state.userId,
            postId: e
          }
          post_bookmark(newUser)
            .then((res) => {
              this.componentDidMount()
            }
            ).catch((er) =>{
              console.log("Erorr", er);
            })
        }
      }
      fetch_recent_post(){
        fetch(url+"/recent")
        .then(res => res.json())
        .then(recent_posts =>
        this.setState( {recent_posts}
        )
        ).catch((er) =>{
          console.log("Erorr", er);
        })
      }
      fetch_post(){
        fetch(url+ this.props.location.pathname)
        .then(res => res.json())
        .then(posts =>
        this.setState( {posts} , () =>
            this.setState({postTitle: posts[0].post.title}),
            this.setState({comments: posts[0].comments}),
        )
        ).catch((er) =>{
          console.log("Erorr", er);
        })
      }
      post_post_star(e){
        const newUser = {
          userId: this.state.userId,
          postId: e
        }
        post_star(newUser)
          .then((res) => {
            this.fetch_post_star(e)}
          ).catch((er) =>{
            console.log("Erorr", er);
          })
      }

      fetch_post_star(e){
        const newUser = {
          search: e
        }
        
        get_post_star(newUser)
          .then(post_star =>
          this.setState( {post_star_count: post_star}    
          )
          ).catch((er) =>{
            console.log("Erorr", er);
          })
      }

    componentDidMount() {
      if(localStorage.user_token){
        this.setState({
          userId: jwt_decode(localStorage.user_token).id,
          name: jwt_decode(localStorage.user_token).name,
          email_id: jwt_decode(localStorage.user_token).email_id
        })
        this.fetch_bookmark(jwt_decode(localStorage.user_token).id)
      }
      this.fetch_recent_post();
      this.fetch_post();
      
    }
  render() {    

    let filters ;
      if(this.state.posts.length == 0 || this.state.posts.length == undefined ){
          filters = [];
      }else{
         filters = this.state.posts.filter(visitor => {
          return (
            visitor
          );
        });
      }
      let shareUrl = window.location.href;
      let title = null;
      if(this.state.posts.length != 0){
        title = this.state.posts[0].post.title.replace(/[-]/g, " ") + " #Code-ML" + " \n Look This Awesome Post here.";
      }
      
      let givenUserId;
      if(localStorage.user_token != undefined){
        givenUserId = jwt_decode(localStorage.user_token).id
      }else{
        givenUserId=null;
      }
      let recent_posts_filter;
        if(this.state.recent_posts.length == 0 || this.state.recent_posts.length == undefined ){
            recent_posts_filter = [];
        }else{
        recent_posts_filter = this.state.recent_posts.filter(visitor => {
            return (
            visitor
            );
        });
        }
        
    return (
      <div style={{height:"100%"}} className="post">
        <Helmet>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <meta property="og:title" content={filters.length != 0 ? filters[0].post.title.replace(/[-]/g, " "): "Code-ML"} />
            <meta name="description" content={filters.length != 0 ? filters[0].post.title.replace(/[-]/g, " "): "Code-ML"}/>
            <meta property="og:image" content={filters.length != 0 ? filters[0].post.cover_img : logo  }/>
            <meta property="og:url" content={`https://code-ml.com/${filters.length != 0 ? filters[0].post.title: ""}`}/>
            <meta name="twitter:card" content={filters.length != 0 ? filters[0].post.title.replace(/[-]/g, " "): "Code-ML"}/>
            <link rel="shortcut icon" href={"https://code-ml.com/" + logo}/>
            <link rel="canonical" href={window.location.href} />
            <title>{filters.length != 0 ? filters[0].post.title.replace(/[-]/g, " "): "Code-ML"}</title>
        </Helmet>
            <Navbar p={this.props}/>
            <div className="post__M" >
            {filters.map((po, i) => ( 
              <div key={i} className="post__bigMain">
                <div  className="post__leftSide">
                  <LEFTSTARSIDEBAR post={po.post}/>
                </div>
                <div className="post__main"> 
                  <div className="post__star">
                    <div style={{display:"flex", flexDirection:"column"}}>
                      <div>
                        <IconContext.Provider value={{ color: "white",size:"25px", className: "global-class-name" }}>
                          <TiLightbulb style={{cursor:"pointer"}} onClick={()=>(this.post_post_star(po.post.id))} />
                          <span style={{color:"white"}}>{po.post.stars >this.state.post_star_count ?po.post.stars: this.state.post_star_count }</span>
                        </IconContext.Provider>
                      </div>
                      <div style={{marginTop:"5px"}}>
                        <IconContext.Provider value={{ color: "white" ,size:"25px", className: "global-class-name" }}>
                          <GiBookmark style={{cursor:"pointer", color:"white"}} onClick={()=>(this.post_bookmark(po.post.id))}/>
                          <span style={{color:"white"}}>{this.state.posts.length == 0 ? this.props.post.bookmark_count : this.state.posts[0].post.bookmark_count}</span>
                        </IconContext.Provider>
                      </div>
                    </div>
                  </div>
                    <Container className="post__container">
                        <h2 className="post__title"><span>{po.post.title.replace(/[-]/g, " ")}</span></h2>
                        <div>
                        <Image style={{margin:"20px", marginLeft:"10px"}} className="post__adminImg" alt="avatar" src={po.post.user.thumbnail == null?"https://www.w3schools.com/howto/img_avatar.png": po.post.user.thumbnail } width="40px" height="40px" className="logoone" roundedCircle = "true" /> 
                        <span style={{color:"white"}}>By {po.post.user.name}  /  <Moment format="MMM D YYYY" withTitle date={po.post.createdAt} /></span>
                        </div>
                        <div style={{marginBottom:"10px"}} className="post__img">
                        <Image style={{margin:"0px"}} alt="" src={po.post.cover_img} width="100%" height="100%" className="logoone" /> 
                        </div>
                        <div style={{width:"100%", height:"100%"}}> {parse(po.post.body)}</div>
                        <div style={{color:"#F2AA4CFF",fontFamily:"montserrat", fontSize:"20px"}} >
                          tags - {po.post.tags.map(function(object, i){
                            return <span key={i}><a href={`https://code-ml.com/tag/${object}`} style={{color:"#F2AA4CFF", margin:"5px"}}><Badge pill style={{backgroundColor:"black"}}>#{object}</Badge></a></span>;
                          })}
                        </div>
                        <div style={{display:"flex", justifyContent:"center", marginTop:"20px"}}>
                          <div style={{margin:"2px"}}>
                          <FacebookShareButton
                            url={"www.google.com"}
                            quote={"Please checkout our post"}
                          >
                            <FacebookIcon size={32} round />
                          </FacebookShareButton>
                            <FacebookShareCount url={"www.google.com"} >
                              {count => count}
                            </FacebookShareCount>
                            </div>
                          <div style={{margin:"2px"}}>
                              <TwitterShareButton
                                url={shareUrl}
                                title={title}
                              >
                                <TwitterIcon size={32} round />
                              </TwitterShareButton>
                              <div className="Demo__some-network__share-count">&nbsp;</div>
                            </div>
                            <div style={{margin:"2px"}}>
                              <TelegramShareButton
                                url={shareUrl}
                                title={title}
                              >
                                <TelegramIcon size={32} round />
                              </TelegramShareButton>
                            </div>
                            <div style={{margin:"2px"}}>
                              <WhatsappShareButton
                                url={shareUrl}
                                title={title}
                                separator=":: "
                              >
                                <WhatsappIcon size={32} round />
                              </WhatsappShareButton>
                            </div>
                            <div style={{margin:"2px"}}>
                              <LinkedinShareButton url={shareUrl} >
                                <LinkedinIcon size={32} round />
                              </LinkedinShareButton>
                            </div>
                        </div>
                        <div className="post__star__small">
                          <div className="post__star__small__inner">
                            <div style={{margin:"10px"}}>
                              <IconContext.Provider value={{ color: "white",size:"25px", className: "global-class-name" }}>
                                <TiLightbulb style={{cursor:"pointer"}} onClick={()=>(this.post_post_star(po.post.id))} />
                                <span style={{color:"white"}}>{po.post.stars >this.state.post_star_count ?po.post.stars: this.state.post_star_count }</span>
                              </IconContext.Provider>
                            </div>
                            <div style={{margin:"10px"}}>
                              <IconContext.Provider value={{ color: "white" ,size:"25px", className: "global-class-name" }}>
                                <GiBookmark style={{cursor:"pointer", color:"white"}} onClick={()=>(this.post_bookmark(po.post.id))}/>
                                <span style={{color:"white"}}>{this.state.posts.length == 0 ? this.props.post.bookmark_count : this.state.posts[0].post.bookmark_count}</span>
                              </IconContext.Provider>
                            </div>
                          </div>
                        </div>
                        <hr className="post__hr"/>
                        <div><p style={{textAlign:"center", fontSize:"30px",fontFamily:"bembo", color:"#F2AA4CFF"}}>Thank You</p></div>
                        <div style={{marginBottom:"20px"}}>
                        <Card className="post__adminCard">
                            <Card.Body className="post__adminCardBody">
                                  <div style={{display:"flex", justifyContent:"start", marginTop:"10px"}}>
                                  <Image
                                  style={{margin:"20px"}}
                                    alt=""
                                    src={po.post.user.thumbnail == null?"https://www.w3schools.com/howto/img_avatar.png": po.post.user.thumbnail }
                                    width="60px"
                                    height="60px"
                                    className="logoone"
                                    roundedCircle = "true"
                                  />  
                                  <Card.Title style={{marginTop:"20px", marginLeft:"5px"}}>
                                    <div>
                                      <a href={"/user/"+po.post.user.username} style={{color:"white",fontFamily:"charter"}}>{po.post.user.name}</a><br/>
                                      <a href={"/user/"+po.post.user.username} style={{color:"white",fontFamily:"charter", fontSize:"15px"}}>@{po.post.user.username}</a><br/>
                                      <p style={{color:"white",fontFamily:"charter", fontSize:"18px", marginTop:"5px"}}>Bio - {po.post.user.bio}</p> 
                                    </div>                        
                                  </Card.Title>
                                  <div className="post__adminCardFooter">
                                    <IconContext.Provider value={{ color: "white" ,size:"20px", className: "global-class-name" }}>
                                      <a href={this.state.github == null? '':'https://github.com/'+ this.state.github} target="_blank" aria-label="Github" style={{marginRight:"10px"}}><GoMarkGithub/></a>
                                    </IconContext.Provider>
                                    <IconContext.Provider value={{ color: "white" ,size:"20px", className: "global-class-name" }}>
                                      <a href={this.state.linkedin == null? '':this.state.linkedin} style={{marginRight:"10px"}} aria-label="Linkedin" target="_blank"><FaLinkedin/></a>
                                    </IconContext.Provider>
                                    <IconContext.Provider value={{ color: "white" ,size:"20px", className: "global-class-name" }}>
                                      <a href={this.state.twitter == null? '':'https://twitter.com/'+ this.state.twitter} aria-label="Twitter" style={{marginRight:"10px"}} target="_blank"><FaTwitter/></a>
                                    </IconContext.Provider>
                                    <IconContext.Provider value={{ color: "white" ,size:"20px", className: "global-class-name" }}>
                                      <a href={this.state.personal_link == null? '':this.state.personal_link} style={{marginRight:"10px"}} aria-label="Link" target="_blank"><FaLink/></a>
                                    </IconContext.Provider>
                                  </div>
                                  </div>
                            </Card.Body>
                        </Card>
                        <Card className="post__postCommentCard">
                          <Card.Body >
                          <Form onSubmit={this.onSubmit}>
                              <Form.Group controlId="exampleForm.ControlTextarea1" >
                              <Form.Label style={{ color: "white"}}>Comment</Form.Label>
                              <Form.Control 
                              style={{backgroundColor:"#101820FF", borderColor:"#F2AA4CFF", color:"white"}}
                              as="textarea" 
                              name="comment_text"
                              placeholder="Discuss Here" 
                              value={this.state.comment_text}
                              onChange={this.onChange}
                              rows="3" />
                              </Form.Group>
                              <Button 
                                  variant="primary" 
                                  type="submit" 
                                  style={{float:"right", backgroundColor:"#F2AA4CFF", border:"0", color:"black"}} onClick={()=>(this.setState({postTitle:po.post.title,postId: po.post.id}))}  >
                                  Submit
                              </Button>
                          </Form>
                          </Card.Body>
                      </Card>
                        </div>
                        <div style={{backgroundColor:"#101820FF"}} className="post__commentCard">
                      {this.state.comments.map(function(object, i){
                        return <COMMENT p={object} key={i}/> 
                      })}
                    </div>
                    
                    </Container>
                    
                </div>
              </div>
              ))}
            <div style={{ marginLeft:"0px"}}>
            {(this.state.userId == null || this.state.bookmark.length == 0) ? <ListGroup.Item className="noBookmark">
            <p style={{color:"#F2AA4CFF", display:"flex", justifyContent:"center",letterSpacing:"0.2em",lineHeight:"1.4em",fontWeight:"bold",fontFamily:"charter"}}>Bookmarks</p>
            <p style={{color:"white", display:"flex", justifyContent:"center", fontSize:"15px"}}>No Bookmark</p>
            </ListGroup.Item> :
          <ListGroup.Item className="post__bookmark">
                <p style={{display:"flex", justifyContent:"center",letterSpacing:"0.2em",lineHeight:"1.4em",fontWeight:"bold",fontFamily:"charter", color:"#F2AA4CFF"}}>Bookmarks</p>
                {this.state.bookmark.map((rp, i)=> (
                <a key={i} style={{color:"white", textDecoration:"none"}} href={"https://code-ml.com/post/"+ rp.post.title}>
                    <Card style={{ width: '18rem', marginBottom:"5px", borderWidth:"2px", borderColor:"black", backgroundColor:"#101820FF"}}>
                    <Card.Body>
                    <Card.Title style={{fontFamily:"charter"}}><a style={{color:"#F2AA4CFF", textDecoration:"none"}} href={"https://code-ml.com/post/"+ rp.post.title}>{rp.post.title.replace(/[-]/g, " ")} </a></Card.Title>
                        <Card.Subtitle className="mb-2 text-muted">
                        <div style={{color:"white",fontFamily:"charter"}}><span style={{color:"white",fontFamily:"normal"}}><Moment format="D MMM YYYY" withTitle date={rp.post.createdAt} /></span></div>
                        </Card.Subtitle>
                        <footer style={{justifyContent:"space-between", display:"flex"}}>
                            <div>
                                <IconContext.Provider value={{ color: "white",size:"20px", className: "global-class-name" }}>
                                    <TiLightbulb/>
                                    <span style={{paddingRight:"10px", paddingLeft:"5px"}}>{rp.post.stars == null ? 0 : rp.post.stars}</span>
                                </IconContext.Provider>
                                <IconContext.Provider value={{ color: "white" ,size:"20px", className: "global-class-name" }}>
                                    <MdInsertComment/>
                                    <span style={{paddingRight:"10px", paddingLeft:"5px"}}>{rp.post.comment_count}</span>
                                </IconContext.Provider>
                            </div>
                            <div>
                                <IconContext.Provider value={{ color: "white" ,size:"20px", className: "global-class-name" }}>
                                <GiBookmark style={{cursor:"pointer"}} onClick={()=>(this.state.userId==null ? alert("Login to bookmark !!"):this.post_bookmark(rp.post.id))}/>
                                </IconContext.Provider>
                                <span style={{paddingLeft:"5px"}}>{rp.post.bookmark_count}</span>
                            </div>
                        </footer>
                    </Card.Body>
                    </Card>
                    </a>
                ))}
                </ListGroup.Item>
          }
            <ListGroup.Item className="post__recentSideBar">
                <p style={{display:"flex", justifyContent:"center",letterSpacing:"0.2em",lineHeight:"1.4em",fontWeight:"bold",fontFamily:"charter", color:"#F2AA4CFF"}}>Recent Posts</p>
                {recent_posts_filter.map((rp, i)=> (
                    <Card key={i} style={{ width: '18rem', marginBottom:"5px", borderWidth:"2px", borderColor:"black", backgroundColor:"#101820FF"}}>
                    <Card.Body>
                        <Card.Title style={{fontFamily:"charter"}}><a style={{color:"#F2AA4CFF", textDecoration:"none"}} href={"https://code-ml.com/post/"+ rp.title}>{rp.title.replace(/[-]/g, " ")} </a></Card.Title>
                        <Card.Subtitle className="mb-2 text-muted">
                        <div style={{color:"white",fontFamily:"charter"}}>{rp.user.name+" . "}<span style={{color:"white",fontFamily:"normal"}}><Moment format="D MMM YYYY" withTitle date={rp.createdAt} /></span></div>
                        </Card.Subtitle>
                        <Card.Text style={{color:"white"}}>
                            {rp.read_time.text}
                        </Card.Text>
                        <footer style={{justifyContent:"space-between", display:"flex"}}>
                            <div>
                                <IconContext.Provider value={{ color: "white",size:"20px", className: "global-class-name" }}>
                                    <TiLightbulb/>
                                    <span style={{color: "white",paddingRight:"10px", paddingLeft:"5px"}}>{rp.stars == null ? 0 : rp.stars}</span>
                                </IconContext.Provider>
                                <IconContext.Provider value={{ color: "white" ,size:"20px", className: "global-class-name" }}>
                                    <MdInsertComment/>
                                    <span style={{color: "white",paddingRight:"10px", paddingLeft:"5px"}}>{rp.comment_count}</span>
                                </IconContext.Provider>
                            </div>
                            <div>
                                <IconContext.Provider value={{ color: "white" ,size:"20px", className: "global-class-name" }}>
                                    <GiBookmark style={{cursor:"pointer"}} onClick={()=>(this.post_bookmark(rp.id))}/>
                                    <span style={{color: "white", paddingLeft:"5px"}}>{rp.bookmark_count}</span>
                                </IconContext.Provider>
                            </div>
                        </footer>
                    </Card.Body>
                    </Card>
                   
                ))}
                </ListGroup.Item>
            <ListGroup/>
        </div>
            </div>
            <FOOTER/>
      </div>
    );
  }
}

export default POSTMAIN;
