import React, { Component } from "react";
import {
    Row,
    Col,
    Figure,
    Card,
  } from "react-bootstrap";

import Moment from 'react-moment';
import { get_reply_comment, post_reply_comment_text} from '../funtion/comment_funtion'
import jwt_decode from 'jwt-decode'

class REPLYCOMMENT extends Component {
    constructor(props) {
        super(props);
        this.state = {
            reply_comment:[],
            reply_comment_text:"",
            inputLinkClicked: false
        };
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }
    onChange(e) {
      this.setState({ [e.target.name]: e.target.value })
    }

    onSubmit(e) {
        e.preventDefault()
        
      const newUser = {
        text: this.state.reply_comment_text,
        commentId:this.props.p.id,
        userId: this.state.userId
      }
      post_reply_comment_text(newUser).then(res => {
        this.setState({reply_comment_text:""})
        this.close_reply_comment();
        this.fetch_reply_commets(this.props.p.id);
      })
    }

    reply_comment() {
        this.setState({
          inputLinkClicked: true
        })
      }
    close_reply_comment() {
    this.setState({
        inputLinkClicked: false
    })
    }
    fetch_reply_commets(e){
      const newUser = {
        id: e
      }
      get_reply_comment(newUser)
        .then(reply_comment =>
        this.setState( {reply_comment}           
        )
        );
    }
    componentDidMount() {
      if(localStorage.user_token){
        this.setState({
          userId: jwt_decode(localStorage.user_token).id,
          name: jwt_decode(localStorage.user_token).name,
          email_id: jwt_decode(localStorage.user_token).email_id
        })
      }
      this.fetch_reply_commets(this.props.p.id);
    }
  render() {
    return (
        <div className="replyComment">
            <Card className="replyComment__card">
                <Card.Body >
                <Row>
                    <Col  md={1} style={{paddingRight:"0"}}>
                    <Figure>
                    <Figure.Image 
                            style={{margin:"0", paddingRight:"5px"}}
                            width={60}
                            height={70}
                            alt="171x180"
                            roundedCircle
                            src={ this.props.p.userId == null?"https://www.w3schools.com/howto/img_avatar.png": this.props.p.user.thumbnail }
                            />
                    </Figure>
                    </Col>
                    <Col  md={11} style={{paddingLeft:"5px"}}>
                    <div>
                    <a href={"/user/"+this.props.p.user.username}><h6 style={{color:"#808080",fontFamily:"montserrat"}}>{this.props.p.user.name+" . "}<span style={{color:"#808080",fontFamily:"normal"}}><Moment format="D MMM YYYY" withTitle date={this.props.p.createdAt} /></span></h6></a>
                    </div>
                    <a href={"/user/"+this.props.p.user.username} style={{color:"white",fontFamily:"charter", fontSize:"15px"}}>@{this.props.p.user.username}</a><br/>

                    <Card.Text style={{ marginBottom:"20px",backgroundColor:"#101820FF" , color:"white", padding:"10px"}}>
                        
                        {this.props.p.text}
                        
                    </Card.Text>
                    </Col>
                </Row>
                </Card.Body>
            </Card>
          </div>
          
    );
  }
}

export default REPLYCOMMENT;




