import React, { Component } from "react";
import {
    Row,
    Col,
    Figure,
    Form,
    Card,
    Button,
    Badge
  } from "react-bootstrap";

import Moment from 'react-moment';
import { get_reply_comment, post_reply_comment_text} from '../funtion/comment_funtion'
import { comment_star, get_comment_star } from '../funtion/star_function'
import jwt_decode from 'jwt-decode'
import NEWCOMMENT from "./reply_comment";
class COMMENT extends Component {
    constructor(props) {
        super(props);
        this.state = {
            reply_comment:[],
            reply_comment_text:"",
            inputLinkClicked: false,
            comment_star_count:"",
            name: '',
            email_id: '',
            userId: null
        };
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }
    onChange(e) {
      this.setState({ [e.target.name]: e.target.value })
    }

    onSubmit(e) {
        e.preventDefault()
        
      const newUser = {
        text: this.state.reply_comment_text,
        commentId:this.props.p.id,
        userId: this.state.userId
      }
      post_reply_comment_text(newUser).then(res => {
        this.setState({reply_comment_text:""})
        this.close_reply_comment();
        this.fetch_reply_commets(this.props.p.id);
      })
    }
    
    post_comment_star(e){
      const newUser = {
        userId: this.state.userId,
        commentId: e,
        reply_commentId: null
      }
      comment_star(newUser)
        .then((res) => {
          this.fetch_comment_star(e)}
        );
    }
    fetch_comment_star(e){
      const newUser = {
        search: e
      }
      
      get_comment_star(newUser)
        .then(comment_star =>
          this.setState( {comment_star_count: comment_star}    
        )
        );
    }
    reply_comment() {
        this.setState({
          inputLinkClicked: true
        })
      }
    close_reply_comment() {
    this.setState({
        inputLinkClicked: false
    })
    }
    fetch_reply_commets(e){
      const newUser = {
        id: e
      }
      get_reply_comment(newUser)
        .then(reply_comment =>
        this.setState( {reply_comment}         
        )
        );
    }
    componentDidMount() {
      if(localStorage.user_token){
        this.setState({
          userId: jwt_decode(localStorage.user_token).id,
          name: jwt_decode(localStorage.user_token).name,
          email_id: jwt_decode(localStorage.user_token).email_id
        })
      }
      this.fetch_reply_commets(this.props.p.id);
      this.fetch_comment_star(this.props.p.id);
    }
  render() {
    let show_comment_box = this.state.inputLinkClicked;
    let show = this.reply_comment.bind(this);
    let close = this.close_reply_comment.bind(this);
    return (
        <div style={{backgroundColor:"#101820FF"}}>
          {this.props.p.user==null? <div></div> : 
          <Card className="comment">
                <Card.Body  >
                <Row>
                    <Col  md={1} style={{paddingRight:"0"}}>
                    <Figure>
                    <Figure.Image 
                            style={{margin:"0", paddingRight:"5px"}}
                            width={60}
                            height={70}
                            alt="171x180"
                            roundedCircle
                            src={ this.props.p.user.thumbnail == null?"https://www.w3schools.com/howto/img_avatar.png": this.props.p.user.thumbnail }
                            />
                    </Figure>
                    </Col>
                    <Col  md={11} style={{paddingLeft:"5px"}}>
                    <div style={{display:"flex", justifyContent:"space-between"}}>
                    <a href={"/user/"+this.props.p.user.username}><h6 style={{color:"#808080",fontFamily:"montserrat"}}>{this.props.p.user.name+" . "}<span style={{color:"#808080",fontFamily:"normal"}}><Moment format="D MMM YYYY" withTitle date={this.props.p.createdAt} /></span></h6></a>
                    <div>
                    </div>
                    </div>
                    <a href={"/user/"+this.props.p.user.username} style={{color:"white",fontFamily:"charter", fontSize:"15px"}}>@{this.props.p.user.username}</a><br/>
                    <Card.Text style={{backgroundColor:"#101820FF", marginBottom:"20px", padding:"10px", color:"white"}}>
                        
                        {this.props.p.text}
                        
                        </Card.Text>
                        {this.state.reply_comment.length == 0? <div></div>: 
                        <div style={{paddingTop:"20px", paddingBottom:"10px", backgroundColor:"#101820FF"}} >
                        {this.state.reply_comment.map(function(object, i){
                          if(object.user != null){
                            return <NEWCOMMENT p={object} key={i}/>
                          }
                          
                        })}
                        </div>
                        }
                        <div>
                        {
                            show_comment_box ?
                            <Card className="replyBox__card">
                                <div style={{margin:"5px"}}>
                                <Badge pill style={{float:"right", margin:"3px", cursor:"pointer", color:"#F2AA4CFF"}} onClick={close} >X</Badge>
                                </div>
                            <Card.Body >
                                <Form onSubmit={this.onSubmit}>
                                <Form.Group controlId="exampleForm.ControlTextarea1">
                                    <Form.Control 
                                    style={{backgroundColor:"#101820FF", borderColor:"#F2AA4CFF",color:"white"}}
                                    as="textarea" 
                                    placeholder="Reply" 
                                    name="reply_comment_text"
                                    rows="3" 
                                    value={this.state.reply_comment_text}
                                    onChange={this.onChange}
                                    />
                                    <Button variant="primary" type="submit" style={{marginTop:"7px", float:"right", backgroundColor:"#F2AA4CFF", border:"0"}} >
                                    Submit
                                    </Button>
                                </Form.Group>
                                </Form>
                            </Card.Body>
                            </Card>
                            :
                            <a style={{float:"right", color:"white", marginTop:"10px"}} onClick={show}>Reply</a>
                        }
                        </div>
                    
                    
                    </Col>
                </Row>
                </Card.Body>
            </Card>}
            
          </div>
          
    );
  }
}

export default COMMENT;




