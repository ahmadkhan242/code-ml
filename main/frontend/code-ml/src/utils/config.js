var url = ""
if(process.env.NODE_ENV == "development"){
    url = "http://localhost:4000"
}

if(process.env.NODE_ENV == "production"){
    url = "https://code-ml.com/api"
}


module.exports = {url};
