const db = require('../models');
const generateId = require('../helper/generateId');
const Sequelize = require("sequelize");
const readingTime = require('reading-time');

module.exports = {
  create_post: async (post_obj) => {
    try {
      const post_response = await db.post.create({
        id: generateId.generateId(),
        ...post_obj,
      });
      return post_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  get_all_post: async whereObj => {
    try {
      let query = {
        include: [
          {
            model: db.user, as: 'user',
            attributes: ['id','name','email_id','thumbnail', 'username']
          }
        ],
        attributes: ["id","title","shares","tags","topics","createdAt","body","cover_img","category",
          [Sequelize.literal('(SELECT SUM(star_count) FROM "post_stars" WHERE "postId"="post"."id" GROUP BY "postId")'), 'stars'],
          [Sequelize.literal('(SELECT COUNT(*) FROM "comments" WHERE "postId"="post"."id" )'), 'comment_count'],
          [Sequelize.literal('(SELECT COUNT(*) FROM "bookmarks" WHERE ("postId"="post"."id" AND "bookmarked"=true))'), 'bookmark_count']
        ],
        order: [['createdAt','DESC']]
      }; 
      const post_response = await db.post.findAll(query);
      return post_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  get_post_by_title: async whereObj => {
    try {
       
      let query = {
        where: whereObj,
        include: [
          {
            model: db.user, as: 'user',
            attributes: ["id","name","email_id","country","createdAt","username","thumbnail","bio", "github", "linkedin", "personal_link", "twitter", "work" ]
          }
        ],
        attributes: ["id","title","body","shares","tags","topics","createdAt","cover_img",
          [Sequelize.literal('(SELECT SUM(star_count) FROM "post_stars" WHERE "postId"="post"."id" GROUP BY "postId")'), 'stars'],
          [Sequelize.literal('(SELECT COUNT(*) FROM "comments" WHERE "postId"="post"."id")'), 'comment_count'],
          [Sequelize.literal('(SELECT COUNT(*) FROM "bookmarks" WHERE ("postId"="post"."id" AND "bookmarked"=true))'), 'bookmark_count']
        ]
      };       
      const post_response = await db.post.findOne(query);
      return post_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  get_recent_post: async whereObj => {
    try {
       
      let query = {
        limit: 4,
        include: [
          {
            model: db.user, as: 'user',
            attributes: ['id','name','email_id', 'thumbnail', 'username']
          }
        ],
        attributes: ["id","title","body","shares","tags","topics","createdAt","cover_img",
          [Sequelize.literal('(SELECT SUM(star_count) FROM "post_stars" WHERE "postId"="post"."id" GROUP BY "postId")'), 'stars'],
          [Sequelize.literal('(SELECT COUNT(*) FROM "comments" WHERE "postId"="post"."id")'), 'comment_count'],
          [Sequelize.literal('(SELECT COUNT(*) FROM "bookmarks" WHERE ("postId"="post"."id" AND "bookmarked"=true))'), 'bookmark_count']
        ],
        order: [['createdAt','DESC']]
      };       
      const post_response = await db.post.findAll(query);
      return post_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  get_post_by_query: async whereObj => {
    try {
       let type= whereObj.type;
      let query = {
        where: {
          [type] :{
            [Sequelize.Op.contains]: [whereObj.search]
          } 
        },
        include: [
          {
            model: db.user, as: 'user',
            attributes: ['id','name','email_id', 'thumbnail', 'username']
          }
        ],
        attributes: ["id","title","shares","tags","topics","createdAt","body","cover_img",
          [Sequelize.literal('(SELECT SUM(star_count) FROM "post_stars" WHERE "postId"="post"."id" GROUP BY "postId")'), 'stars'],
          [Sequelize.literal('(SELECT COUNT(*) FROM "comments" WHERE "postId"="post"."id")'), 'comment_count'],
          [Sequelize.literal('(SELECT COUNT(*) FROM "bookmarks" WHERE ("postId"="post"."id" AND "bookmarked"=true))'), 'bookmark_count']
        ],
        order: [['createdAt','DESC']],
        limit: whereObj.limit
      };       
      const post_response = await db.post.findAll(query);
      return post_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  update_post: async (post_obj) => {
    try {
      const post_response = await db.post.update({
        ...post_obj
      }, {
        where: {
          id: post_obj.id
        }
      }, );
      return post_response;
    } catch (error) {
      console.log(error);
    }
  }
}