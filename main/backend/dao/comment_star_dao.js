const db = require('../models');
const generateId = require('../helper/generateId');
const Sequelize = require("sequelize");

module.exports = {
  create_comment_star: async (comment_star_obj) => {
    try {
      const comment_star_response = await db.comment_star.create({
        id: generateId.generateId(),
        ...comment_star_obj,
      });
      return comment_star_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  get_all: async whereObj => {
    try {
      const comment_star_response = await db.comment_star.findAll({
        where: whereObj,
        attributes: [[Sequelize.fn('SUM', Sequelize.col('star_count')), 'comment_star_count']],
        group: 'commentId'
      });
      return comment_star_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  get_comment_star: async whereObj => {
    try {
      const select = [
        {commentId: whereObj.commentId},
        {userId: whereObj.userId},
        {replyCommentId: whereObj.reply_commentId}
      ];
      const comment_star_response = await db.comment_star.findOne({
        where: {[Sequelize.Op.and]: select} 
      });
      return comment_star_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  get_count: async whereObj => {
    try {
      const comment_star_response = await db.comment_star.findAll({
        attributes: [[Sequelize.fn('COUNT', Sequelize.col('commentId')), 'comment_star_count']],
        group: 'commentId'
            });
      return comment_star_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  get_all_comment_star: async whereObj => {
    try {
      const star_response = await db.comment_star.findAll({
        where: whereObj
      });
      return star_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  update_comment_star: async (comment_star_obj) => {
    try {
      const comment_star_response = await db.comment_star.update({
        ...comment_star_obj
      }, {
        where: {
          id: comment_star_obj.id 
        }
      }, );
      return comment_star_response;
    } catch (error) {
      console.log(error);
    }
  }
}