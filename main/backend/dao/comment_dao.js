const db = require('../models');
const generateId = require('../helper/generateId');

module.exports = {
  create_comment: async (comment_obj) => {
    try {
      const comment_response = await db.comment.create({
        id: generateId.generateId(),
        ...comment_obj,
      });
      return comment_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  get_all_post_comment: async whereObj => {
    try {
      const comment_response = await db.comment.findAll({
        where: whereObj,
        include: [
          {model: db.user, as:"user"}
        ],
        order: [['createdAt','DESC']]
      });
      return comment_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  get_comment: async whereObj => {
    try {
      const comment_response = await db.comment.findOne({
        where: whereObj
      });
      return comment_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  update_comment: async (comment_obj) => {
    try {
      const comment_response = await db.comment.update({
        ...comment_obj
      }, {
        where: {
          id: comment_obj.id
        }
      }, );
      return comment_response;
    } catch (error) {
      console.log(error);
    }
  }
}