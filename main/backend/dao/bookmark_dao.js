const db = require('../models');
const generateId = require('../helper/generateId');
const Sequelize = require("sequelize");

module.exports = {
  create_bookmark: async (bookmark_obj) => {
    try {
      const bookmark_response = await db.bookmark.create({
        id: generateId.generateId(),
        ...bookmark_obj,
      });
      return bookmark_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  get_all_post_bookmark: async whereObj => {
    try {
      const bookmark_response = await db.bookmark.findAll({
        where: whereObj
        ,include: [
          {
            model: db.post, as: 'post',
            attributes: ["id","title","shares","tags","topics","createdAt","body","cover_img",
            [Sequelize.literal('(SELECT SUM(star_count) FROM "post_stars" WHERE "postId"="post"."id" GROUP BY "postId")'), 'stars'],
            [Sequelize.literal('(SELECT COUNT(*) FROM "comments" WHERE "postId"="post"."id")'), 'comment_count'],
            [Sequelize.literal('(SELECT COUNT(*) FROM "bookmarks" WHERE ("postId"="post"."id" AND "bookmarked"=true))'), 'bookmark_count']]
          }
        ]
      });
      return bookmark_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  get_bookmark: async whereObj => {
    try {
        const select = [
            {postId: whereObj.postId},
            {userId: whereObj.userId}
        ];
      const bookmark_response = await db.bookmark.findOne({
        where: {[Sequelize.Op.and]: select} 
      });
      return bookmark_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  update_bookmark: async (bookmark_obj) => {
    try {
      const bookmark_response = await db.bookmark.update({
        ...bookmark_obj
      }, {
        where: {
          id: bookmark_obj.id 
        }
      }, );
      return bookmark_response;
    } catch (error) {
      console.log(error);
    }
  }
}