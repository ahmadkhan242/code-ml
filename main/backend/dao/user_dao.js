const db = require('../models');
const generateId = require('../helper/generateId');
const Sequelize = require("sequelize");

module.exports = {
  create_user: async (user_obj) => {
    try {
      const user_response = await db.user.create({
        id: generateId.generateId(),
        ...user_obj,
      });
      return user_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  get_all: async whereObj => {
    try {
      const user_response = await db.user.findAll({
        order: [
          ['name', 'ASc']
        ],
      });
      return user_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  get_user: async whereObj => {
    try {
      let query = {
        where: 
          whereObj 
        ,
        attributes: ["id","name","email_id","country","createdAt","username","thumbnail","bio", "github", "linkedin", "personal_link", "twitter", "work","password"]
      };   
      const user_response = await db.user.findOne(query);
      return user_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  get_user_by_id: async whereObj => {
    try {
      let query = {
        where: {
          whereObj 
        },
        attributes: ["id","title","shares","tags","topics","createdAt","body","cover_img",
          [Sequelize.literal('(SELECT COUNT(*) FROM "post_stars" WHERE "userId"="user"."id")'), 'stars'],
          [Sequelize.literal('(SELECT COUNT(*) FROM "comments" WHERE "userId"="user"."id")'), 'comment_count'],
          [Sequelize.literal('(SELECT COUNT(*) FROM "bookmarks" WHERE "userId"="user"."id")'), 'bookmark_count']
        ]
      };   
      const user_response = await db.user.findOne(query);
      return user_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  userprofile: async whereObj => {
    try {
      let query = {
        where: 
          whereObj 
        ,
        attributes: ["id","name","email_id","country","createdAt","username","thumbnail","bio", "github", "linkedin", "personal_link", "twitter", "work","password",
        [Sequelize.literal('(SELECT COUNT(*) FROM "post_stars" WHERE "userId"="user"."id")'), 'stars'],
        [Sequelize.literal('(SELECT COUNT(*) FROM "comments" WHERE "userId"="user"."id")'), 'comment_count'],
        [Sequelize.literal('(SELECT COUNT(*) FROM "bookmarks" WHERE "userId"="user"."id")'), 'bookmark_count']]
      };   
      const user_response = await db.user.findOne(query);
      return user_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  update_user: async (user_obj) => {
    try {
      const user_response = await db.user.update({
        ...user_obj
      }, {
        where: {
          id: user_obj.userId
        }
      }, );
      return user_response;
    } catch (error) {
      console.log(error);
    }
  }
}