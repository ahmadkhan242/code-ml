const db = require('../models');
const generateId = require('../helper/generateId');

module.exports = {
  create: async (image_data_obj) => {
    try {
      const image_data_response = await db.image_data.create({
        id: generateId.generateId(),
        ...image_data_obj,
      });
      return image_data_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  get_all: async whereObj => {
    try {
      const image_data_response = await db.image_data.findAll({
        order: [
          ['name', 'ASc']
        ],
      });
      return image_data_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  get_image_data: async whereObj => {
    try {
      const image_data_response = await db.image_data.findOne({
        where: whereObj
      });
      return image_data_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }
}