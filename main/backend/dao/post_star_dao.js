const db = require('../models');
const generateId = require('../helper/generateId');
const Sequelize = require("sequelize");

module.exports = {
  create_post_star: async (post_star_obj) => {
    try {
      const post_star_response = await db.post_star.create({
        id: generateId.generateId(),
        ...post_star_obj,
      });
      return post_star_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  get_all: async whereObj => {
    try {
      const post_star_response = await db.post_star.findAll({
        where: whereObj,
        attributes: [[Sequelize.fn('SUM', Sequelize.col('star_count')), 'post_star_count']],
        group: 'postId'
      });
      return post_star_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  get_post_star: async whereObj => {
    try {
      const select = [
        {postId: whereObj.postId},
        {userId: whereObj.userId}
      ];
      const post_star_response = await db.post_star.findOne({
        where: {[Sequelize.Op.and]: select} 
      });
      return post_star_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  get_count: async whereObj => {
    try {
      const post_star_response = await db.post_star.findAll({
        attributes: [[Sequelize.fn('COUNT', Sequelize.col('postId')), 'post_star_count']],
        group: 'postId'
            });
      return post_star_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  get_all_post_star: async whereObj => {
    try {
      const star_response = await db.post_star.findAll({
        where: whereObj
      });
      return star_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  update_post_star: async (post_star_obj) => {
    try {
      const post_star_response = await db.post_star.update({
        ...post_star_obj
      }, {
        where: {
          id: post_star_obj.id 
        }
      }, );
      return post_star_response;
    } catch (error) {
      console.log(error);
    }
  }
}