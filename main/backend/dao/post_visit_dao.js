const db = require('../models');
const generateId = require('../helper/generateId');
const Sequelize = require("sequelize");

module.exports = {
  create_post_visit: async (post_visit_obj) => {
    try {
      const post_visit_response = await db.post_visit.create({
        id: generateId.generateId(),
        ...post_visit_obj,
      });
      return post_visit_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  get_all: async whereObj => {
    try {
      const post_visit_response = await db.post_visit.findAll({
        where: whereObj,
        order: [
          ['createdAt', 'ASc']
        ],
      });
      return post_visit_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  get_post_visit: async whereObj => {
    try {
      const select = [
        {postId: whereObj.postId},
        {userId: whereObj.userId}
    ];
      const post_visit_response = await db.post_visit.findOne({
        where: {[Sequelize.Op.and]: select} 
      });
      return post_visit_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  update_post_visit: async (post_visit_obj) => {
    try {
      const post_visit_response = await db.post_visit.update({
        ...post_visit_obj
      }, {
        where: {
          id: post_visit_obj.id
        }
      }, );
      return post_visit_response;
    } catch (error) {
      console.log(error);
    }
  }
}