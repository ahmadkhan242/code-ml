const db = require('../models');
const generateId = require('../helper/generateId');

module.exports = {
  create_reply_comment: async (reply_comment_obj) => {
    try {
      const reply_comment_response = await db.reply_comment.create({
        id: generateId.generateId(),
        ...reply_comment_obj,
      });
      return reply_comment_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  get_all_post_reply_comment: async whereObj => {
    try {
      const reply_comment_response = await db.reply_comment.findAll({
        where: whereObj,
        include: [
          {model: db.user, as:"user"}
        ]
      });
      return reply_comment_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  get_reply_comment: async whereObj => {
    try {
      const reply_comment_response = await db.reply_comment.findOne({
        where: whereObj
      });
      return reply_comment_response;
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  update_reply_comment: async (reply_comment_obj) => {
    try {
      const reply_comment_response = await db.reply_comment.update({
        ...reply_comment_obj
      }, {
        where: {
          id: reply_comment_obj.id
        }
      }, );
      return reply_comment_response;
    } catch (error) {
      console.log(error);
    }
  }
}