module.exports = (sequelize, Sequelize) => {
    const comment = sequelize.define("comment", {
      id: {
        type: Sequelize.UUID,
        primaryKey: true
      },
      text: {
        type: Sequelize.TEXT
      }
    }, {
      timestamps: true
    });
    comment.associate = models =>{
      models.comment.belongsTo(models.post);
      models.comment.belongsTo(models.user);
    }
    return comment;
  };

  
// COMMAND IN SQL
//   CREATE TABLE comment ( id UUID,
//                     text TEXT,
//                     PRIMARY KEY (id));
