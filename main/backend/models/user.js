module.exports = (sequelize, Sequelize) => {
  const user = sequelize.define("user", {
    id: {
      type: Sequelize.UUID,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING
    },
    email_id: {
      type: Sequelize.STRING
    },
    password: {
      type: Sequelize.STRING
    },
    country:{
      type:Sequelize.STRING
    },
    googleId:{
      type: Sequelize.STRING
    },
    facebookId:{
      type: Sequelize.STRING
    },
    githubId:{
      type: Sequelize.STRING
    },
    username:{
      type: Sequelize.STRING
    },
    thumbnail: {
      type: Sequelize.STRING,
      allowNull: true
    },
    bio: {
      type: Sequelize.STRING,
      allowNull: true
    },
    github: {
      type: Sequelize.STRING,
      allowNull: true
    },
    linkedin: {
      type: Sequelize.STRING,
      allowNull: true
    },
    personal_link: {
      type: Sequelize.STRING,
      type: Sequelize.STRING
    },
    twitter: {
      type: Sequelize.STRING,
      allowNull: true
    },
    work: {
      type: Sequelize.STRING,
      allowNull: true
    }
  }, {
    timestamps: true
  });

  return user;
};