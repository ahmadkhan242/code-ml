module.exports = (sequelize, Sequelize) => {
    const bookmark = sequelize.define("bookmark", {
      id: {
        type: Sequelize.UUID,
        primaryKey: true
      },
      bookmarked:{
        type: Sequelize.BOOLEAN
      }
    }, {
      timestamps: true
    });
    bookmark.associate = models => {
      // associations can be defined here
      models.bookmark.belongsTo(models.post);
      models.bookmark.belongsTo(models.user);
    };
    return bookmark;
  };
// COMMAND IN SQL
//   CREATE TABLE star ( id UUID,first_name VARCHAR(100),
//                     second_name VARCHAR(100),
//                     email VARCHAR(100),
//                     password VARCHAR(100));