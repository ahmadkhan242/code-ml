module.exports = (sequelize, Sequelize) => {
    const comment_star = sequelize.define("comment_star", {
      id: {
        type: Sequelize.UUID,
        primaryKey: true
      },
      star_count:{
        type: Sequelize.INTEGER,
        defaultValue: 0
      }
    }, {
      timestamps: true
    });
    comment_star.associate = models => {
      // associations can be defined here
      models.comment_star.belongsTo(models.comment);
      models.comment_star.belongsTo(models.reply_comment);
      models.comment_star.belongsTo(models.user);
    };
    return comment_star;
  };
// COMMAND IN SQL
//   CREATE TABLE comment_star ( id UUID,first_name VARCHAR(100),
//                     second_name VARCHAR(100),
//                     email VARCHAR(100),
//                     password VARCHAR(100));