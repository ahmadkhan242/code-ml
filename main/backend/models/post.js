var showdown  = require('showdown'),
    converter = new showdown.Converter()

module.exports = (sequelize, Sequelize) => {
    const post = sequelize.define("post", {
      id: {
        type: Sequelize.UUID,
        primaryKey: true
      },
      cover_img:{
        type: Sequelize.STRING
      },
      body: {
        type: Sequelize.TEXT,
        get() {
          return converter.makeHtml(this.getDataValue('body'));
        }
      },
      title: {
        type: Sequelize.TEXT
      },
      tags: {
        type: Sequelize.ARRAY(Sequelize.STRING) 
      },
      topics: {
        type: Sequelize.ARRAY(Sequelize.STRING) 
      },
      shares: {
        type: Sequelize.INTEGER
      },
      category:{
        type: Sequelize.ARRAY(Sequelize.STRING)
      },
      discription: {
        type: Sequelize.STRING
      }
    }, {
      timestamps: true
    });
    post.associate = models => {
      // associations can be defined here
      models.post.belongsTo(models.user);
    };
    return post;
  };

  


// COMMAND IN SQL
//   CREATE TABLE post ( id UUID,first_name VARCHAR(100),
//                     second_name VARCHAR(100),
//                     email VARCHAR(100),
//                     password VARCHAR(100), 
//                     PRIMARY KEY (id));
