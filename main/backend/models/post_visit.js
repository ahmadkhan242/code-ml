module.exports = (sequelize, Sequelize) => {
    const post_visit = sequelize.define("post_visit", {
      id: {
        type: Sequelize.UUID,
        primaryKey: true
      },
      visited: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      }
    }, {
      timestamps: true
    });
    post_visit.associate = models =>{
      models.post_visit.belongsTo(models.post);
      models.post_visit.belongsTo(models.user);
    }
    return post_visit;
  };
// COMMAND IN SQL
//   CREATE TABLE post_visit ( id UUID,
//                     visited BOOLEAN,
//                     PRIMARY KEY (id));
