module.exports = (sequelize, Sequelize) => {
    const reply_comment = sequelize.define("reply_comment", {
      id: {
        type: Sequelize.UUID,
        primaryKey: true
      },
      text: {
        type: Sequelize.TEXT
      }
    }, {
      timestamps: true
    });
    reply_comment.associate = models =>{
      models.reply_comment.belongsTo(models.comment);
      models.reply_comment.belongsTo(models.user);
    }
    return reply_comment;
  };
