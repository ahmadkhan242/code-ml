module.exports = (sequelize, Sequelize) => {
    const post_star = sequelize.define("post_star", {
      id: {
        type: Sequelize.UUID,
        primaryKey: true
      },
      star_count:{
        type: Sequelize.INTEGER,
        defaultValue: 0
      }
    }, {
      timestamps: true
    });
    post_star.associate = models => {
      // associations can be defined here
      models.post_star.belongsTo(models.post);
      models.post_star.belongsTo(models.user);
    };
    return post_star;
  };
// COMMAND IN SQL
//   CREATE TABLE post_star ( id UUID,first_name VARCHAR(100),
//                     second_name VARCHAR(100),
//                     email VARCHAR(100),
//                     password VARCHAR(100));