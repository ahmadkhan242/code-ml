const Joi = require("joi");

module.exports = {
    post_validation: async post_Obj => {
        const schema = Joi.object().keys({
            title: Joi.string().required(),
            body: Joi.string().required(),
            tags: Joi.array().required(),
            topics: Joi.array().required(),
            shares: Joi.number().required(),
            cover_img: Joi.string().required(),
            category: Joi.array().required(),
            userId: Joi.string().required(),
            discription: Joi.string().required()
        });
        return Joi.validate(post_Obj, schema);
    },
    update_post_validation: async post_Obj => {
        const schema = Joi.object().keys({
            id:Joi.string().required(),
            title: Joi.string().required(),
            body: Joi.string().required(),
            tags: Joi.array().required(),
            topics: Joi.array().required(),
            shares: Joi.number().required(),
            cover_img: Joi.string().required(),
            category: Joi.array().required(),
            userId: Joi.string().required(),
            discription : Joi.string().required()
        });
        return Joi.validate(post_Obj, schema);
    },
};