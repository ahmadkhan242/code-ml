const Joi = require("joi");

module.exports = {
    comment_validation: async comment_Obj => {
        const schema = Joi.object().keys({
            text: Joi.string().required(),
            postId: Joi.string().required(),
            userId: Joi.string().required()
        });
        return Joi.validate(comment_Obj, schema);
    },
    update_comment_validation: async comment_Obj => {
        const schema = Joi.object().keys({
            ide:Joi.string().required(),
            text: Joi.string().required(),
            postId: Joi.string().required(),
            userId: Joi.string().required()
        });
        return Joi.validate(comment_Obj, schema);
    },
};