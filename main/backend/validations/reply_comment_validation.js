const Joi = require("joi");

module.exports = {
    reply_comment_validation: async reply_comment_Obj => {
        const schema = Joi.object().keys({
            text: Joi.string().required(),
            commentId: Joi.string().required(),
            userId: Joi.string().required()
        });
        return Joi.validate(reply_comment_Obj, schema);
    }
};