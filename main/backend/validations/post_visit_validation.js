const Joi = require("joi");

module.exports = {
    post_visit_validation: async post_visit_Obj => {
        const schema = Joi.object().keys({
            userId: Joi.string().required(),
            postId: Joi.string().required()
        });
        return Joi.validate(post_visit_Obj, schema);
    }
};