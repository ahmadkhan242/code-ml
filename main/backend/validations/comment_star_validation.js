const Joi = require("joi");

module.exports = {
    comment_star_validation: async comment_star_Obj => {
        const schema = Joi.object().keys({
            userId: Joi.string().allow([null]),
            commentId: Joi.string().allow([null]),
            reply_commentId: Joi.string().allow([null])
        });
        return Joi.validate(comment_star_Obj, schema);
    }
};