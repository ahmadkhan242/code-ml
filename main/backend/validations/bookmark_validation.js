const Joi = require("joi");

module.exports = {
    bookmark_validation: async bookmark_Obj => {
        const schema = Joi.object().keys({
            postId: Joi.string().required(),
            userId: Joi.string().required()
        });
        return Joi.validate(bookmark_Obj, schema);
    }
};