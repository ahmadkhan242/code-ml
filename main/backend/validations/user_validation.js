const Joi = require("joi");

module.exports = {
    google_user_validation: async user_Obj => {
        const schema = Joi.object().keys({
            name: Joi.string().required(),
            thumbnail: Joi.string().required(),
            googleId: Joi.string().required(),
            email_id: Joi.string().email().required()
        });
        return Joi.validate(user_Obj, schema);
    },
    facebook_user_validation: async user_Obj => {
        const schema = Joi.object().keys({
            name: Joi.string().required(),
            thumbnail: Joi.string().required(),
            facebookId: Joi.string().required(),
            email_id: Joi.string().email().required()
        });
        return Joi.validate(user_Obj, schema);
    },
    update_user_validation: async user_Obj => {
        const schema = Joi.object().keys({
            userId: Joi.string().required(),
            name: Joi.string().required(),
            email_id: Joi.string().email().required(),
            country: Joi.string().required(),
            bio: Joi.string().allow([null]),
            work: Joi.string().allow([null]),
            github: Joi.string().allow([null]),
            linkedin: Joi.string().allow([null]),
            personal_link: Joi.string().allow([null]),
            twitter: Joi.string().allow([null]),
            username: Joi.string().required()
        });
        return Joi.validate(user_Obj, schema);
    }

};