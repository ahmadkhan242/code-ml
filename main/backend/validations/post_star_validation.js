const Joi = require("joi");

module.exports = {
    post_star_validation: async post_star_Obj => {
        const schema = Joi.object().keys({
            userId: Joi.string().allow([null]),
            postId: Joi.string().required()
        });
        return Joi.validate(post_star_Obj, schema);
    }
};