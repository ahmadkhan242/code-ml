require("dotenv").config();

module.exports = {
  google_clientID:process.env.google_clientID,
  google_clientSecret:process.env.google_clientSecret,
  SECRET_KEY: process.env.SECRET_KEY,
  isDev: process.env.IS_DEV ,
  database: process.env.DATABASE_NAME,
  username: process.env.DB_USER,
  password: process.env.PASSWORD,
  databaseConfigs: {
    host: process.env.HOSTNAME || "localhost",
    dialect: "postgres",
    port: process.env.DB_PORT || "5432",
    dialectOptions: {
      ssl: process.env.NODE_ENV == "production"
    }
  },
};
