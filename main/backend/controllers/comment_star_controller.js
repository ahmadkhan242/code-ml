const Services = require('../services');
const validation_star = require('../validations/comment_star_validation');

module.exports = {
  create_comment_star: async (req, res) => {
    try {
      const error_messages = await validation_star.comment_star_validation(req.body);
      if (error_messages.error != null) {
        return res.json({
          status: 400,
          message: error_messages.error.details[0].message
        });
      }
      const comment_star_Obj = await Services.comment_star_service.create_comment_star(req.body);

      return res.json(comment_star_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },
  get_comment_star_all: async (req, res) => {
    try {

      const star_Obj = await Services.comment_star_service.get_comment_star_all(req.params.id);

      return res.json(star_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },
  update_star: async (req, res) => {
    try {
      const error_messages = await validation_star.update_star_validation(req.body);
      if (error_messages.error != null) {
        return res.json({
          status: 400,
          message: error_messages.error.details[0].message
        });
      }
      const star_Obj = await Services.star_service.update_star(req.body);

      return res.json(star_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },

}