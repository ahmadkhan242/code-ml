const Services = require('../services');
const validation_user = require('../validations/user_validation');

module.exports = {
  create_user: async (req, res) => {
    try {
      const error_messages = await validation_user.user_validation(req.body);
      if (error_messages.error != null) {
        return res.json({
          status: 400,
          message: error_messages.error.details[0].message
        });
      }
      console.log(Services, "SERVICE");
      const user_Obj = await Services.user_service.create_user(req.body);

      return res.json(user_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },
  update_user: async (req, res) => {
    try {
      const error_messages = await validation_user.user_validation(req.body);
      if (error_messages.error != null) {
        return res.json({
          status: 400,
          message: error_messages.error.details[0].message
        });
      }
      const user_Obj = await Services.user_service.update_user(req.body);

      return res.json(user_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },
  create_google_user: async (body) => {
    try {
      const user_Obj = await Services.user_service.create_google_user(body);

      return user_Obj.data.toJSON();

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },
  get_user_by_username: async (req, res) => {
    try {
      const user_Obj = await Services.user_service.get_user_by_username(req.params.username);

      return res.json(user_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },
  login_user: async (req, res) => {
    try {

      const user_Obj = await Services.user_service.login_user(req.body);

      return res.json(user_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },
  userprofile: async (req, res) => {
    try {

      const user_Obj = await Services.user_service.userprofile(req.body);

      return res.json(user_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },
  update_user: async (req, res) => {
    try {
      const error_messages = await validation_user.update_user_validation(req.body);
      if (error_messages.error != null) {
        return res.json({
          status: 400,
          message: error_messages.error.details[0].message
        });
      }
      const user_Obj = await Services.user_service.update_user(req.body);

      return res.json(user_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },
  google_auth: async (req, res) => {
    try {
      const error_messages = await validation_user.google_user_validation(req.body);
      if (error_messages.error != null) {
        return res.json({
          status: 400,
          message: error_messages.error.details[0].message
        });
      }
      const user_Obj = await Services.user_service.google_auth(req.body);

      return res.json(user_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },
  facebook_auth: async (req, res) => {
    try {
      const error_messages = await validation_user.facebook_user_validation(req.body);
      if (error_messages.error != null) {
        return res.json({
          status: 400,
          message: error_messages.error.details[0].message
        });
      }
      const user_Obj = await Services.user_service.facebook_auth(req.body);

      return res.json(user_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  }
}