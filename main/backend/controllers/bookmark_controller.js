const Services = require('../services');
const validation_bookmark = require('../validations/bookmark_validation');

module.exports = {
  create_bookmark: async (req, res) => {
    try {
      const error_messages = await validation_bookmark.bookmark_validation(req.body);
      if (error_messages.error != null) {
        return res.json({
          status: 400,
          message: error_messages.error.details[0].message
        });
      }
      const bookmark_Obj = await Services.bookmark_service.create_bookmark(req.body);

      return res.json(bookmark_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },
  get_bookmark: async (req, res) => {
    try {

      const bookmark_Obj = await Services.bookmark_service.get_bookmark(req.body);

      return res.json(bookmark_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },
  get_all_bookmark: async (req, res) => {
    try {

      const bookmark_Obj = await Services.bookmark_service.get_all_bookmark(req.params.id);

      return res.json(bookmark_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  }
}