const Services = require('../services');
const validation_post = require('../validations/post_validation');

module.exports = {
  create_post: async (req, res) => {
    try {
      const error_messages = await validation_post.post_validation(req.body);
      if (error_messages.error != null) {
        return res.json({
          status: 400,
          message: error_messages.error.details[0].message
        });
      }
      const post_Obj = await Services.post_service.create_post(req.body);

      return res.json(post_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },
  get_all_post: async (req, res) => {
    try {

      const post_Obj = await Services.post_service.get_all_post();

      return res.json(post_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },
  get_content_post: async (req, res) => {
    try {

      const post_Obj = await Services.post_service.get_content_post();

      return res.json(post_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },
  get_post_by_title: async (req, res) => {
    try {
      const post_Obj = await Services.post_service.get_post_by_title(req.params.title);

      return res.json(post_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },
  update_post: async (req, res) => {
    try {
      const error_messages = await validation_post.update_post_validation(req.body);
      if (error_messages.error != null) {
        return res.json({
          status: 400,
          message: error_messages.error.details[0].message
        });
      }
      const post_Obj = await Services.post_service.update_post(req.body);

      return res.json(post_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },

}