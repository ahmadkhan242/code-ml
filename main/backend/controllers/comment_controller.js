const Services = require('../services');
const validation_comment = require('../validations/comment_validation');

module.exports = {
  create_comment: async (req, res) => {
    try {
      const error_messages = await validation_comment.comment_validation(req.body);
      if (error_messages.error != null) {
        return res.json({
          status: 400,
          message: error_messages.error.details[0].message
        });
      }
      const comment_Obj = await Services.comment_service.create_comment(req.body);

      return res.json(comment_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },
  get_comment: async (req, res) => {
    try {

      const comment_Obj = await Services.comment_service.get_comment(req.body);

      return res.json(comment_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },
  update_comment: async (req, res) => {
    try {
      const error_messages = await validation_comment.update_comment_validation(req.body);
      if (error_messages.error != null) {
        return res.json({
          status: 400,
          message: error_messages.error.details[0].message
        });
      }
      const comment_Obj = await Services.comment_service.update_comment(req.body);

      return res.json(comment_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },

}