const Services = require('../services');
// const validation_image_data = require('../validations/image_data_validation');

module.exports = {
    create_image_data: async (req, res) => {
    try {
    //   const error_messages = await validation_image_data.post_validation_image_data(req.body);
    //   if (error_messages.error != null) {
    //     return res.json({
    //       status: 400,
    //       message: error_messages.error.details[0].message
    //     });
    //   }
      const image_data_Obj = await Services.image_data_service.create_image_data(req.body);

      return res.json(image_data_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  }
}