const Services = require('../services');
const validation_post = require('../validations/post_validation');

module.exports = {
  get_post_by_tag: async (req, res) => {
    try {

      const post_Obj = await Services.query_service.get_post_by_tag(req.params);

      return res.json(post_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },
  get_post_by_topic: async (req, res) => {
    try {
      const post_Obj = await Services.query_service.get_post_by_topic(req.params);

      return res.json(post_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },
  get_post_by_category: async (req, res) => {
    try {
      const post_Obj = await Services.query_service.get_post_by_category(req.params);

      return res.json(post_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },
  get_recent_post: async (req, res) => {
    try {
      const post_Obj = await Services.query_service.get_recent_post();

      return res.json(post_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  }
  

}