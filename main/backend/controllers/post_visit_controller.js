const Services = require('../services');
const validation_post_visit = require('../validations/post_visit_validation');

module.exports = {
  create_post_visit: async (req, res) => {
    try {
      const error_messages = await validation_post_visit.post_visit_validation(req.body);
      if (error_messages.error != null) {
        return res.json({
          status: 400,
          message: error_messages.error.details[0].message
        });
      }
      const post_visit_Obj = await Services.post_visit_service.create_post_visit(req.body);

      return res.json(post_visit_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },
  get_post_visit: async (req, res) => {
    try {

      const post_visit_Obj = await Services.post_visit_service.get_post_visit(req.body);

      return res.json(post_visit_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  }
}