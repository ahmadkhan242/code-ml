const Services = require('../services');
const validation_star = require('../validations/post_star_validation');

module.exports = {
  create_post_star: async (req, res) => {
    try {
      const error_messages = await validation_star.post_star_validation(req.body);
      if (error_messages.error != null) {
        return res.json({
          status: 400,
          message: error_messages.error.details[0].message
        });
      }
      const post_star_Obj = await Services.post_star_service.create_post_star(req.body);

      return res.json(post_star_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },
  get_post_star_all: async (req, res) => {
    try {

      const star_Obj = await Services.post_star_service.get_post_star_all(req.params.id);

      return res.json(star_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },
  update_star: async (req, res) => {
    try {
      const error_messages = await validation_star.update_star_validation(req.body);
      if (error_messages.error != null) {
        return res.json({
          status: 400,
          message: error_messages.error.details[0].message
        });
      }
      const star_Obj = await Services.star_service.update_star(req.body);

      return res.json(star_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },

}