const Services = require('../services');
const validation_reply_comment = require('../validations/reply_comment_validation');

module.exports = {
  create_reply_comment: async (req, res) => {
    try {
      const error_messages = await validation_reply_comment.reply_comment_validation(req.body);
      if (error_messages.error != null) {
        return res.json({
          status: 400,
          message: error_messages.error.details[0].message
        });
      }
      const reply_comment_Obj = await Services.reply_comment_service.create_reply_comment(req.body);

      return res.json(reply_comment_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },
  get_all_reply_comment: async (req, res) => {
    try {

      const reply_comment_Obj = await Services.reply_comment_service.get_all_reply_comment(req.params.id);

      return res.json(reply_comment_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },
  update_reply_comment: async (req, res) => {
    try {
      const error_messages = await validation_reply_comment.update_reply_comment_validation(req.body);
      if (error_messages.error != null) {
        return res.json({
          status: 400,
          message: error_messages.error.details[0].message
        });
      }
      const reply_comment_Obj = await Services.reply_comment_service.update_reply_comment(req.body);

      return res.json(reply_comment_Obj);

    } catch (error) {
      console.log(error);
      return res.json({
        status: 500,
        message: error.message
      });
    }
  },

}