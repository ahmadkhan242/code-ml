const Dao = require("../dao");

module.exports = {
    create_post_star: async (post_star_data) => {
        try {
            let post_star_response
            if(post_star_data.userId == null || post_star_data.userId == ''){
                // const not_user = {
                //     userId: null,
                //     postId: post_star_data.postId
                // }
                 post_star_response = await Dao.post_star_dao.get_post_star(post_star_data);
                if( post_star_response == null ){
                    search_data = {
                        postId: post_star_data.postId,
                        star_count: 1
                    }
                    post_star_response = await Dao.post_star_dao.create_post_star(search_data);
                }else{
                    search_data = {
                        id: post_star_response.dataValues.id,
                        star_count: post_star_response.dataValues.star_count + 1
                    }
                    post_star_response = await Dao.post_star_dao.update_post_star(search_data);                
                }
            }else{
                 post_star_response = await Dao.post_star_dao.get_post_star(post_star_data);
                if( post_star_response == null ){
                    search_data = {
                        postId: post_star_data.postId,
                        userId: post_star_data.userId,
                        star_count: 1
                    }
                    post_star_response = await Dao.post_star_dao.create_post_star(search_data);
                }else{
                    search_data = {
                        id: post_star_response.dataValues.id,
                        star_count: post_star_response.dataValues.star_count + 1
                    }
                    post_star_response = await Dao.post_star_dao.update_post_star(search_data);                
                }
            }
            return {
                data: post_star_response,
                message: "Success"
            }
        } catch (error) {
            console.log("Error in star Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    },
    get_star: async (star_data) => {
        try {
            const star_response = await Dao.star_dao.get_star(star_data);
            if (star_response == 0 || star_response == 'undefined') {
                return {
                    data: star_response,
                    message: "star is not found"
                };
            }
            return {
                data: star_response,
                message: "Success"
            };
        } catch (error) {
            console.log("Error in star Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    },
    get_post_star_all: async (star_data) => {
        try {
            const d ={
                postId: star_data
            }
            const star_response = await Dao.post_star_dao.get_all(d);
            if (star_response == 0 || star_response == 'undefined') {
                return {
                    data: star_response,
                    message: "star is not found"
                };
            }
            return {
                data: star_response[0].dataValues.post_star_count,
                message: "Success"
            };
        } catch (error) {
            console.log("Error in star Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    },
    update_star: async (star_data) => {
        try {
            const star_response = await Dao.star_dao.get_star(star_data);
            if (star_response == 0 || star_response == 'undefined') {
                return {
                    data: star_response,
                    message: "star is not found"
                };
            }
            const star_updated_response = await Dao.star_dao.update_star(star_data);

            return {
                data: star_updated_response,
                message: "Success"
            };
        } catch (error) {
            console.log("Error in star Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    }
}