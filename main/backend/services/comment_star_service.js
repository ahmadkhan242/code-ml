const Dao = require("../dao");

module.exports = {
    create_comment_star: async (comment_star_data) => {
        try {
            let comment_star_response
            
            if(comment_star_data.userId == null || comment_star_data.userId == ''){
                 comment_star_response = await Dao.comment_star_dao.get_comment_star(comment_star_data);
                if( comment_star_response == null ){
                    search_data = {
                        star_count: 1,
                        ...comment_star_data
                    }
                    comment_star_response = await Dao.comment_star_dao.create_comment_star(search_data);
                }else{
                    search_data = {
                        id: comment_star_response.dataValues.id,
                        star_count: comment_star_response.dataValues.star_count + 1
                    }
                    comment_star_response = await Dao.comment_star_dao.update_comment_star(search_data);                
                }
            }else{
                 comment_star_response = await Dao.comment_star_dao.get_comment_star(comment_star_data);
                if( comment_star_response == null ){
                    search_data = {
                        star_count: 1,
                        ...comment_star_data
                    }
                    comment_star_response = await Dao.comment_star_dao.create_comment_star(search_data);
                }else{
                    search_data = {
                        id: comment_star_response.dataValues.id,
                        star_count: comment_star_response.dataValues.star_count + 1
                    }
                    comment_star_response = await Dao.comment_star_dao.update_comment_star(search_data);                
                }
            }
            return {
                data: comment_star_response,
                message: "Success"
            }
        } catch (error) {
            console.log("Error in star Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    },
    get_star: async (star_data) => {
        try {
            const star_response = await Dao.star_dao.get_star(star_data);
            if (star_response == 0 || star_response == 'undefined') {
                return {
                    data: star_response,
                    message: "star is not found"
                };
            }
            return {
                data: star_response,
                message: "Success"
            };
        } catch (error) {
            console.log("Error in star Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    },
    get_comment_star_all: async (star_data) => {
        try {
            const d ={
                commentId: star_data
            }
            const star_response = await Dao.comment_star_dao.get_all(d);
            if (star_response == 0 || star_response == 'undefined') {
                return {
                    data: star_response,
                    message: "star is not found"
                };
            }
            return {
                data: star_response[0].dataValues.comment_star_count,
                message: "Success"
            };
        } catch (error) {
            console.log("Error in star Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    },
    update_star: async (star_data) => {
        try {
            const star_response = await Dao.star_dao.get_star(star_data);
            if (star_response == 0 || star_response == 'undefined') {
                return {
                    data: star_response,
                    message: "star is not found"
                };
            }
            const star_updated_response = await Dao.star_dao.update_star(star_data);

            return {
                data: star_updated_response,
                message: "Success"
            };
        } catch (error) {
            console.log("Error in star Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    }
}