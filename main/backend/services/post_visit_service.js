const Dao = require("../dao");

module.exports = {
    create_post_visit: async (post_visit_data) => {
        try {
            let search_data = {
                userId: post_visit_data.userId,
                postId: post_visit_data.postId
            }
            let post_visit_response = await Dao.post_visit_dao.get_post_visit(search_data);
            if( post_visit_response == null ){
                search_data = {
                    userId: post_visit_data.userId,
                    postId: post_visit_data.postId,
                    visited: true
                }
                post_visit_response = await Dao.post_visit_dao.create_post_visit(search_data);
            }else{
                if(post_visit_response.dataValues.visited == false){
                    search_data = {
                        id: post_visit_response.dataValues.id,
                        visited: true
                    }
                    post_visit_response = await Dao.post_visit_dao.update_post_visit(search_data);
                }else if(post_visit_response.dataValues.visited == true){
                    search_data = {
                        id: post_visit_response.dataValues.id,
                        visited: false
                    }
                    post_visit_response = await Dao.post_visit_dao.update_post_visit(search_data);
                }
            }
            return {
                data: post_visit_response,
                message: "Success"
            }
        } catch (error) {
            console.log("Error in post_visit Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    },
    get_post_visit: async (post_visit_data) => {
        try {
            const post_visit_response = await Dao.post_visit_dao.get_post_visit(post_visit_data);
            if (post_visit_response == 0 || post_visit_response == 'undefined') {
                return {
                    data: post_visit_response,
                    message: "post_visit is not found"
                };
            }
            return {
                data: post_visit_response,
                message: "Success"
            };
        } catch (error) {
            console.log("Error in post_visit Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    },
    get_all_post_visit: async (post_visit_data) => {
        try {
            const post_visit_response = await Dao.post_visit_dao.get_all(post_visit_data);
            if (post_visit_response == 0 || post_visit_response == 'undefined') {
                return {
                    data: post_visit_response,
                    message: "post_visit is not found"
                };
            }
            return {
                data: post_visit_response,
                message: "Success"
            };
        } catch (error) {
            console.log("Error in post_visit Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    },
    update_post_visit: async (post_visit_data) => {
        try {
            const post_visit_response = await Dao.post_visit_dao.get_post_visit(post_visit_data);
            if (post_visit_response == 0 || post_visit_response == 'undefined') {
                return {
                    data: post_visit_response,
                    message: "post_visit is not found"
                };
            }
            const post_visit_updated_response = await Dao.post_visit_dao.update_post_visit(post_visit_data);

            return {
                data: post_visit_updated_response,
                message: "Success"
            };
        } catch (error) {
            console.log("Error in post_visit Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    }
}