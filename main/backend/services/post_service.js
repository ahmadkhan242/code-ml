const Dao = require("../dao");
const readingTime = require('reading-time');
const Services = require('../services/query_service');

module.exports = {
    create_post: async (post_data) => {
        try {
            const data = {
                title: post_data.title.replace(/\s/g, "-"),
                body: post_data.body,
                topics: post_data.topics,
                tags: post_data.tags,
                shares: post_data.shares,
                category: post_data.category,
                cover_img: post_data.cover_img,
                userId: post_data.userId
            }
            
            const post_response = await Dao.post_dao.create_post(data);
            return {
                data: post_response,
                message: "Success"
            }
        } catch (error) {
            console.log("Error in post Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    },
    get_post_by_title: async (post_data) => {
        try {
            const data = {
                title: post_data
            }
            const post_response = await Dao.post_dao.get_post_by_title(data);
            if (post_response == 0 || post_response == 'undefined') {
                return {
                    data: post_response,
                    message: "post is not found"
                };
            }
            const comment_response = await Dao.comment_dao.get_all_post_comment({
                postId: post_response.id
            });

            const final_responce = {
                post: post_response,
                comments: comment_response,
            }
            return [final_responce];
        } catch (error) {
            console.log("Error in post Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    },
    get_all_post: async () => {
        try {
            const post_response = await Dao.post_dao.get_all_post();
            if (post_response == 0 || post_response == 'undefined') {
                return {
                    data: post_response,
                    message: "post is not found"
                };
            }
            for(var i=0;i<post_response.length;i++){
                Object.assign(post_response[i].dataValues, {read_time: readingTime(post_response[i].dataValues.body)});
            }
            return post_response;

        } catch (error) {
            console.log("Error in post Servise", error);
            return {
                status: 500,
                message: error.message
            };
        }
    },
    get_content_post: async () => {
        try {
            const ml = await Services.get_post_for_content({search: "Machine Learning"});
            const aml = await Services.get_post_for_content({search: "Applied Machine Learning"});
            const dl = await Services.get_post_for_content({search: "Deep Learning"});
            const qna = await Services.get_post_for_content({search: "Questionnaire"});
            const resource = await Services.get_post_for_content({search: "Recources"});
           
            const final_responce = [ml, aml, dl, qna, resource];
            return final_responce;

        } catch (error) {
            console.log("Error in post Servise", error);
            return {
                status: 500,
                message: error.message
            };
        }
    },
    update_post: async (post_data) => {
        try {
            const post_response = await Dao.post_dao.get_post(post_data);
            if (post_response == 0 || post_response == 'undefined') {
                return {
                    data: post_response,
                    message: "post is not found"
                };
            }
            const post_updated_response = await Dao.post_dao.update_post(post_data);

            return {
                data: post_updated_response,
                message: "Success"
            };
        } catch (error) {
            console.log("Error in post Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    }
}