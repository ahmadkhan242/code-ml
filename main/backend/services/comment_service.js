const Dao = require("../dao");

module.exports = {
    create_comment: async (comment_data) => {
        try {
            const comment_response = await Dao.comment_dao.create_comment(comment_data);
            return {
                data: comment_response,
                message: "Success"
            }
        } catch (error) {
            console.log("Error in comment Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    },
    get_comment: async (comment_data) => {
        try {
            const comment_response = await Dao.comment_dao.get_comment(comment_data);
            if (comment_response == 0 || comment_response == 'undefined') {
                return {
                    data: comment_response,
                    message: "comment is not found"
                };
            }
            return {
                data: comment_response,
                message: "Success"
            };
        } catch (error) {
            console.log("Error in comment Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    },
    get_all_comment: async (comment_data) => {
        try {
            const comment_response = await Dao.comment_dao.get_all(comment_data);
            if (comment_response == 0 || comment_response == 'undefined') {
                return {
                    data: comment_response,
                    message: "comment is not found"
                };
            }
            return {
                data: comment_response,
                message: "Success"
            };
        } catch (error) {
            console.log("Error in comment Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    },
    update_comment: async (comment_data) => {
        try {
            const comment_response = await Dao.comment_dao.get_comment(comment_data);
            if (comment_response == 0 || comment_response == 'undefined') {
                return {
                    data: comment_response,
                    message: "comment is not found"
                };
            }
            const comment_updated_response = await Dao.comment_dao.update_comment(comment_data);

            return {
                data: comment_updated_response,
                message: "Success"
            };
        } catch (error) {
            console.log("Error in comment Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    }
}