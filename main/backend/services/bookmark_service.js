const Dao = require("../dao");

module.exports = {
    create_bookmark: async (bookmark_data) => {
        try {
            
            let search_data = {
                userId: bookmark_data.userId,
                postId: bookmark_data.postId
            }
            let bookmark_response = await Dao.bookmark_dao.get_bookmark(search_data);
            if( bookmark_response == null ){
                search_data = {
                    userId: bookmark_data.userId,
                    postId: bookmark_data.postId,
                    bookmarked: true
                }
                bookmark_response = await Dao.bookmark_dao.create_bookmark(search_data);
            }else{
                if(bookmark_response.dataValues.bookmarked == false){
                    search_data = {
                        id: bookmark_response.dataValues.id,
                        bookmarked: true
                    }
                    bookmark_response = await Dao.bookmark_dao.update_bookmark(search_data);
                }else if(bookmark_response.dataValues.bookmarked == true){
                    search_data = {
                        id: bookmark_response.dataValues.id,
                        bookmarked: false
                    }
                    bookmark_response = await Dao.bookmark_dao.update_bookmark(search_data);
                }
            }
            return {
                data: bookmark_response,
                message: "Success"
            }
        } catch (error) {
            console.log("Error in bookmark Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    },
    get_all_bookmark: async (bookmark_data) => {
        try {
            const d = {
                search: bookmark_data
            }
            const bookmark_response = await Dao.bookmark_dao.get_all_post_bookmark(d);
            if (bookmark_response == 0 || bookmark_response == 'undefined') {
                return {
                    data: bookmark_response.length,
                    message: "bookmark is not found"
                };
            }
            return {
                data: bookmark_response,
                message: "Success"
            };
        } catch (error) {
            console.log("Error in bookmark Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    }
}