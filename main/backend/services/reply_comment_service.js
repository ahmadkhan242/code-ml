const Dao = require("../dao");

module.exports = {
    create_reply_comment: async (reply_comment_data) => {
        try {
            const reply_comment_response = await Dao.reply_comment_dao.create_reply_comment(reply_comment_data);
            return {
                data: reply_comment_response,
                message: "Success"
            }
        } catch (error) {
            console.log("Error in reply_comment Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    },
    get_reply_comment: async (reply_comment_data) => {
        try {
            const reply_comment_response = await Dao.reply_comment_dao.get_reply_comment(reply_comment_data);
            if (reply_comment_response == 0 || reply_comment_response == 'undefined') {
                return {
                    data: reply_comment_response,
                    message: "reply_comment is not found"
                };
            }
            return {
                data: reply_comment_response,
                message: "Success"
            };
        } catch (error) {
            console.log("Error in reply_comment Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    },
    get_all_reply_comment: async (reply_comment_data) => {
        try {
            const query = {
                commentId: reply_comment_data
            }
            const reply_comment_response = await Dao.reply_comment_dao.get_all_post_reply_comment(query);
            if (reply_comment_response == 0 || reply_comment_response == 'undefined') {
                return {
                    data: reply_comment_response,
                    message: "reply_comment is not found"
                };
            }
            return {
                data: reply_comment_response,
                message: "Success"
            };
        } catch (error) {
            console.log("Error in reply_comment Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    },
    update_reply_comment: async (reply_comment_data) => {
        try {
            const reply_comment_response = await Dao.reply_comment_dao.get_reply_comment(reply_comment_data);
            if (reply_comment_response == 0 || reply_comment_response == 'undefined') {
                return {
                    data: reply_comment_response,
                    message: "reply_comment is not found"
                };
            }
            const reply_comment_updated_response = await Dao.reply_comment_dao.update_reply_comment(reply_comment_data);

            return {
                data: reply_comment_updated_response,
                message: "Success"
            };
        } catch (error) {
            console.log("Error in reply_comment Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    }
}