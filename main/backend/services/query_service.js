const Dao = require("../dao");
const readingTime = require('reading-time');

module.exports = {
    get_post_by_tag: async (post_data) => {
        try {
            const payload = {
                type: "tags",
                search: post_data.query
            }
            const post_response = await Dao.post_dao.get_post_by_query(payload);
            if (post_response == 0 || post_response == 'undefined') {
                return {
                    data: post_response,
                    message: "post is not found"
                };
            }
            for(var i=0;i<post_response.length;i++){
                Object.assign(post_response[i].dataValues, {read_time: readingTime(post_response[i].dataValues.body)});
            }
        
        
            return post_response;
        } catch (error) {
            console.log("Error in post Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    },
    get_post_by_topic: async (post_data) => {
        try {
            const payload = {
                type: "topics",
                search: post_data.query
            }
            const post_response = await Dao.post_dao.get_post_by_query(payload);
            if (post_response == 0 || post_response == 'undefined') {
                return {
                    data: post_response,
                    message: "post is not found"
                };
            }
            for(var i=0;i<post_response.length;i++){
                Object.assign(post_response[i].dataValues, {read_time: readingTime(post_response[i].dataValues.body)});
            }
           
            return post_response;
        } catch (error) {
            console.log("Error in post Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    },
    get_post_by_category: async (post_data) => {
        try {
            const payload = {
                type: "category",
                search: post_data.query,
                limit: 1000
            }
            const post_response = await Dao.post_dao.get_post_by_query(payload);
            if (post_response == 0 || post_response == 'undefined') {
                return {
                    data: post_response,
                    message: "post is not found"
                };
            }
            for(var i=0;i<post_response.length;i++){
                Object.assign(post_response[i].dataValues, {read_time: readingTime(post_response[i].dataValues.body)});
            }
           
            return post_response;
        } catch (error) {
            console.log("Error in post Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    },
    get_post_for_content: async (post_data) => {
        try {
            const payload = {
                type: "category",
                search: post_data.search,
                limit: 4
            }
            const post_response = await Dao.post_dao.get_post_by_query(payload);
            if (post_response == 0 || post_response == 'undefined') {
                return {
                    data: post_response,
                    message: "post is not found"
                };
            }
            for(var i=0;i<post_response.length;i++){
                Object.assign(post_response[i].dataValues, {read_time: readingTime(post_response[i].dataValues.body)});
            }
           
            return post_response;
        } catch (error) {
            console.log("Error in post Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    },
    get_recent_post: async (post_data) => {
        try {
            const post_response = await Dao.post_dao.get_recent_post();
            if (post_response == 0 || post_response == 'undefined') {
                return {
                    data: post_response,
                    message: "post is not found"
                };
            }
            for(var i=0;i<post_response.length;i++){
                Object.assign(post_response[i].dataValues, {read_time: readingTime(post_response[i].dataValues.body)});
            }
           
            return post_response;
        } catch (error) {
            console.log("Error in post Servise", error);
            return res.json({
                status: 500,
                message: error.message
            });
        }
    }
  
}