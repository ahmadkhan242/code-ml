const Dao = require("../dao");
const { hashSync, compareSync} = require("bcryptjs");
const jwt = require('jsonwebtoken');
const config = require("../config/config");
const gravatar = require('gravatar');

module.exports = {
    create_user: async (user_data) => {
        try {
            var user = await Dao.user_dao.get_user({email_id: user_data.email_id});
            if(user != undefined || user != null){
                return {
                    data: null,
                    message: "Email Exist"
                }
            }
             user = await Dao.user_dao.get_user({username: user_data.username});
            if(user != undefined || user != null){
                return {
                    data: null,
                    message: "Username Exist"
                }
            }
            const data = {
                name: user_data.name,
                email_id: user_data.email_id,
                password: hashSync(user_data.password),
                country: user_data.country,
                username: user_data.username,
                bio: user_data.bio,
                work: user_data.work,
                github: user_data.github,
                linkedin: user_data.linkedin,
                personal_link: user_data.personal_link,
                twitter: user_data.twitter,
                thumbnail: gravatar.url(user_data.email_id, {s: '200', r: 'pg', d: '404'})
            }
            const user_response = await Dao.user_dao.create_user(data);
            return {
                data: user_response,
                message: "Success"
            }
        } catch (error) {
            console.log("Error in User Servise", error);
            return {
                data: error,
                message: "Error"
            }
        }
    },
    create_google_user: async (user_data) => {
        try {
            const user = await Dao.user_dao.get_user({email_id: user_data.email_id});
            if(user != undefined || user != null){
                return {
                    data: null,
                    message: "Email is used earlier"
                }
            }
            const user_response = await Dao.user_dao.create_user(user_data);
            return {
                data: user_response,
                message: "Success"
            }
        } catch (error) {
            console.log("Error in User Servise", error);
            return {
                data: error,
                message: "Error"
            }
        }
    },
    get_user_by_username: async (user_data) => {
        try {
            const query = {
                username: user_data
            }
            const user_response = await Dao.user_dao.get_user(query);
            if (user_response == 0 || user_response == 'undefined') {
                const final_response = {
                    data: user_response,
                    message: "User is not found"
                }
                return [final_response];
            }
            const final_response_1 = {
                data: user_response,
                message: "Success"
            }
            return final_response_1 ;
        } catch (error) {
            console.log("Error in User Servise", error);
            return {
                data: error,
                message: "Error"
            }
        }
    },
    login_user: async (user_data) => {
        try {
            const user_response = await Dao.user_dao.get_user({email_id: user_data.email_id});
            if (user_response == null || user_response == 'undefined') {
                return {
                    data: user_response,
                    message: "User is not found"
                };
            }
            // console.log(user_response.dataValues,"USEUERU REMJO");
            var is_user = compareSync(user_data.password, user_response.dataValues.password);
            if (is_user) {
                  let token = jwt.sign(user_response.dataValues, config.SECRET_KEY, {
                    expiresIn: 1440
                  })
                  
                  return {
                    data: token,
                    message: "Success"
                };
                
            }
            
            return {
                data: null,
                message: "Wrong password"
            };
        } catch (error) {
            console.log("Error in User Servise", error);
            return {
                data: error,
                message: "Error"
            }
        }
    },
    get_user: async (user_data) => {
        try {
            const user_response = await Dao.user_dao.get_user(user_data);
            if (user_response == 0 || user_response == 'undefined') {
                return {
                    data: user_response,
                    message: "User is not found"
                };
            }
            return {
                data: user_response,
                message: "Success"
            };
        } catch (error) {
            console.log("Error in User Servise", error);
            return {
                data: error,
                message: "Error"
            }
        }
    },
    userprofile: async (user_data) => {
        try {
            const user_response = await Dao.user_dao.userprofile(user_data);
            if (user_response == 0 || user_response == 'undefined') {
                return {
                    data: user_response,
                    message: "User is not found"
                };
            }
            const bookmark_response = await Dao.bookmark_dao.get_all_post_bookmark({userId: user_response.dataValues.id, bookmarked: true});
            const post_star_response = await Dao.post_star_dao.get_all_post_star({userId: user_response.dataValues.id});
            const comment_star_response = await Dao.comment_star_dao.get_all_comment_star({userId: user_response.dataValues.id});
            const post_visit_response = await Dao.post_visit_dao.get_all({userId: user_response.dataValues.id});

           
            const final_user_data = {
                user: user_response.dataValues,
                bookmark: bookmark_response,
                post_star: post_star_response,
                comment_star: comment_star_response,
                post_visit: post_visit_response
            }
            return {
                data: final_user_data,
                message: "Success"
            };
        } catch (error) {
            return {
                data: error,
                message: "Error"
            };
        }
    },
    update_user: async (user_data) => {
        try {
            user = await Dao.user_dao.get_user({username: user_data.username});
            if((user != undefined || user != null) && user["dataValues"].email_id != user_data.email_id){
                return {
                    data: null,
                    message: "Username Exist"
                }
            }
            const user_updated_response = await Dao.user_dao.update_user(user_data);
            const user_response = await Dao.user_dao.get_user({email_id: user_data.email_id});
            if (user_response) {
                let token = jwt.sign(user_response.dataValues, config.SECRET_KEY, {
                  expiresIn: 1440
                })
                return {
                  data: token,
                  logic: "present",
                  message: "Success"
              }
          }
        } catch (error) {
            console.log("Error in User Servise", error);
            return {
                data: error,
                logic: null,
                message: "Error"
            };
        }
    },
    google_auth: async (user_data) => {
        try {
            const user_response = await Dao.user_dao.get_user({email_id: user_data.email_id});
            if (user_response == null || user_response == 'undefined') {
                const create_user_response = await Dao.user_dao.create_user(user_data);
                let token = jwt.sign(create_user_response.dataValues, config.SECRET_KEY, {
                expiresIn: 1440
              })
              return {
                data: token,
                logic: "created",
                message: "Success"
            }
            }
            console.log(user_response.dataValues, "Google AUTH USER DATA");
            if (user_response) {
                  let token = jwt.sign(user_response.dataValues, config.SECRET_KEY, {
                    expiresIn: 1440
                  })
                  return {
                    data: token,
                    logic: "present",
                    message: "Success"
                }
            }
            
        } catch (error) {
            console.log("Error in User Servise", error);
            return {
                data: error,
                logic: null,
                message: "Error"
            }
        }
    },
    facebook_auth: async (user_data) => {
        try {
            const user_response = await Dao.user_dao.get_user({email_id: user_data.email_id});
            if (user_response == null || user_response == 'undefined') {
                const create_user_response = await Dao.user_dao.create_user(user_data);
                let token = jwt.sign(create_user_response.dataValues, config.SECRET_KEY, {
                expiresIn: 1440
              })
              return {
                data: token,
                logic: "created",
                message: "Success"
            }
            }
            if (user_response) {
                  let token = jwt.sign(user_response.dataValues, config.SECRET_KEY, {
                    expiresIn: 1440
                  })
                  return {
                    data: token,
                    logic: "present",
                    message: "Success"
                }
            }
            
        } catch (error) {
            console.log("Error in User Servise", error);
            return {
                data: error,
                logic: null,
                message: "Error"
            }
        }
    },
}