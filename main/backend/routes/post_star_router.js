var post_star_controller = require("../controllers/post_star_controller");

module.exports = router => {
  router
    .route("/post-star")
    .post(post_star_controller.create_post_star)
  router
    .route("/post-starById/:id")
    .get(post_star_controller.get_post_star_all)
    
};