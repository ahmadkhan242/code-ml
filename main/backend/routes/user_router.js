var user_controller = require("../controllers/user_controller");
const passport = require('passport');

module.exports = router => {
    router
        .route("/user/:username")
        .get(user_controller.get_user_by_username)
        .put(user_controller.update_user)
    router
        .route("/login")
        .post(user_controller.login_user)
    router
        .route("/google")
        .post(user_controller.google_auth)
    router
        .route("/facebook")
        .post(user_controller.facebook_auth)
    router
        .route("/edit-profile")
        .post(user_controller.update_user)
    router
        .route("/userprofile")
        .post(user_controller.userprofile)

};