var comment_controller = require("../controllers/comment_controller");

module.exports = router => {
  router
    .route("/comment")
    .post(comment_controller.create_comment)
    .get(comment_controller.get_comment)
    .put(comment_controller.update_comment)
};