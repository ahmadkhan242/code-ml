var post_visit_controller = require("../controllers/post_visit_controller");

module.exports = router => {
  router
    .route("/post-visit")
    .post(post_visit_controller.create_post_visit)
    // .get(post_visit_controller.get_post_visit)
    // .put(post_visit_controller.update_post_visit)
};