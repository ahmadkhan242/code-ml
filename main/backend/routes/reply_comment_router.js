var reply_comment_controller = require("../controllers/reply_comment_controller");

module.exports = router => {
  router
    .route("/reply-comment")
    .post(reply_comment_controller.create_reply_comment)
  router
    .route("/reply-commentById/:id")
    .get(reply_comment_controller.get_all_reply_comment)
};