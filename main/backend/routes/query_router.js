var query_controller = require("../controllers/query_controller");

module.exports = router => {
    router
        .route("/tag/:query")
        .get(query_controller.get_post_by_tag)
    router
        .route("/topic/:query")
        .get(query_controller.get_post_by_topic)
    router
        .route("/category/:query")
        .get(query_controller.get_post_by_category)
    router
        .route("/recent")
        .get(query_controller.get_recent_post)
};