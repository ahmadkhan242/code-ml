var comment_star_controller = require("../controllers/comment_star_controller");

module.exports = router => {
  router
    .route("/comment-star")
    .post(comment_star_controller.create_comment_star)
  router
    .route("/comment-starById/:id")
    .get(comment_star_controller.get_comment_star_all)
};