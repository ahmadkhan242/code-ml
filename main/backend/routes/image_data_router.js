var image_data_controller = require("../controllers/image_data_controller");

module.exports = router => {
  router
    .route("/image-data")
    .post(image_data_controller.create_image_data)
};