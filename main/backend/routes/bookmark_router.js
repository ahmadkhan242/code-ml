var bookmark_controller = require("../controllers/bookmark_controller");

module.exports = router => {
  router
    .route("/bookmark")
    .post(bookmark_controller.create_bookmark)
    .get(bookmark_controller.get_bookmark)
  router
    .route("/bookmarkById/:id")
    .get(bookmark_controller.get_all_bookmark)
};