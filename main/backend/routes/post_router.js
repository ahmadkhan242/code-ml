var post_controller = require("../controllers/post_controller");

module.exports = router => {
  router
    .route("/post")
    .post(post_controller.create_post)
    .get(post_controller.get_all_post)
  router
    .route("/contentPost")
    .get(post_controller.get_content_post)
  router
    .route("/post/:title")
    .get(post_controller.get_post_by_title)
};