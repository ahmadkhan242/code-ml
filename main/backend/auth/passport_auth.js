const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const config = require('../config/config');
const Services = require('../services');
var user_controller = require("../controllers/user_controller");

passport.serializeUser((user, done) => {
    console.log(user, "serializeUser user");
    done(null, user.id);
});

passport.deserializeUser((Data, done) => {
    Services.user_service.get_user({id: Data.id}).then((user) => {
        done(null, user);
    });
});

passport.use(
    new GoogleStrategy({
        // options for google strategy
        clientID: config.google_clientID,
        clientSecret: config.google_clientSecret,
        callbackURL: 'http://localhost:3000/'
    }, (accessToken, refreshToken, profile,email, done) => {
        console.log("==================== USER PROFILE ===================");
        console.log(profile);
        console.log("==================== USER EMAIL ===================");
        console.log(email);
        // check if user already exists in our own db
         Services.user_service.get_user({googleId: email.id})
        .then((currentUser) => {
            if(currentUser.data){
                // already have this user
                console.log('user is: ', currentUser);
                done(null, currentUser);
            } else {
                // if not, create user in our db
                const user_data = {
                    email_id:email._json.email,
                    googleId: email._json.sub,
                    username: email._json.name,
                    thumbnail: email._json.picture
                }
                user_controller.create_google_user(user_data).then((newUser) => {
                    console.log(newUser, "USER CREAYED BY GOOGLE");
                    done(null, newUser);
                }).catch((e)=>{
                    console.log(e,"ERROR ");
                });
            }
        });
    })
);